import React, {useEffect, useState} from 'react';
import {Grid, TextField, Button, FormLabel} from "@material-ui/core";
import FingerprintJS from '@fingerprintjs/fingerprintjs';
import { useHistory } from "react-router-dom";
import useStyles from './Style';
import { Post } from "../../services/api";

const Otp = () => {

    const classes = useStyles();

    let history = useHistory();

    const [code, setCode] = useState('');
    const [codeError, setCodeError] = useState('');

    useEffect(()=>{
        if(code.length === 5) return confirmCode();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [code]);

    const handleCodeChange = (sender) => {
        setCode(sender.target.value)
    };

    const handleError = () => {
        setCodeError("کد تایید وارد شده اشتباه است");
    };

    const confirmCode = async() => {
        const fp = await FingerprintJS.load();
        const fingerPrint = await fp.get();
        const data = await Post(
            'user/auth/loginByMobile',
            {Mobile: localStorage.getItem('loginInput'), Code:code, FingerPrint:fingerPrint.visitorId, FcmToken:null},
            handleError
        );
        localStorage.setItem('token', data.Data.token);
        history.push('/')
    };

    return (
        <Grid container direction="column" alignItems="center">
            <Button onClick={()=>{history.goBack()}}>back</Button>
            <img src={"/logo192.png"} alt="logo" className={classes.logo}/>
            <h1>کد تایید</h1>
            <FormLabel>کد تایید ارسال شده را وارد نمایید</FormLabel>
            <TextField
                variant="outlined"
                className="text-field"
                type="tel"
                fullWidth={true}
                error={!!codeError}
                helperText={codeError}
                onChange={(e)=>handleCodeChange(e)}
            />
            <Button
                variant="contained"
                color="primary"
                fullWidth={true}
                onClick={()=>confirmCode()}
            >
                ادامه
            </Button>
        </Grid>
    );
};

export default Otp;
