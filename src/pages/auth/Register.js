import React, {useState} from 'react';
import {Grid, TextField, Button, FormLabel} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import {validateEmail, validateMobile} from '../../utils/Validator'
import useStyles from './Style';
import { Post } from "../../services/vahid/index";

const Register = () => {
    const classes = useStyles();
    let history = useHistory();
    const [username, setUsername] = useState(localStorage.getItem('loginInput') || '');
    const [password, setPassword] = useState(localStorage.getItem('loginInput') || '');
    const [inputError, setInputError] = useState('');
    const [loading, setLoading] = useState(false);
    const [btnLabel, setBtnLabel] = useState('ورود');
    const [Data,setData]=useState({
    })
    const vahid="4444444444444444444";
    const login =async () => {
        console.log(Data);        
        let x=  await Post(
            {url:  '/user/login'},
             {username: 'user@enfties.com',
              password:'123456'}
         );
    };

    const changeBtnLabel = () => {
        setLoading(false);
        setBtnLabel('تلاش مجدد');
    };

    const loginByMobile = async ()=> {
        localStorage.setItem('loginUsername', username);
        localStorage.setItem('loginPassword', password);
        setLoading(true);
        await Post(
            'user/auth/registerMobile',
            {Mobile: username}
        );
        history.push('/user/login/confirm')
    };

    const handUsernameChange = (sender) => {
        setUsername(sender.target.value);
    };
    const handlePasswordChange = (sender) => {
        setPassword(sender.target.value);
    };
    const handChange = (sender) => {
        const id = sender.target.id;
        const value = sender.target.value;
        setData(preState=> ({
         ...preState,[id]: value}))
//         console.log(Data);
    };
    return (
        <Grid container direction="column" alignItems="center">
            <img src={"/logo.png"} alt="logo" className={classes.logo}/>
            <h1>ورود</h1>
            <FormLabel>نام کاربری</FormLabel>
            <FormLabel></FormLabel>
            <TextField
                type="text"
                id="username"
                variant="outlined"
                className="text-field"
                onChange={(e)=>handChange(e)}
                value={Data.username}
                fullWidth={true}
                error={!!inputError}
                helperText={inputError}
            />
            <FormLabel>رمز عبور</FormLabel>
            <FormLabel></FormLabel>
            <br/>
            <TextField
                variant="outlined"
                className="text-field"
                onChange={(e)=>handChange(e)}
                id="password"
                value={Data.password}
                fullWidth={true}
                error={!!inputError}
                helperText={inputError}
            />
            <FormLabel></FormLabel>
            <Button
                variant="contained"
                color="primary"
                fullWidth={true}
                disabled={loading}
                onClick={()=> login()}
            >
                {btnLabel}
            </Button>
        </Grid>
    );
};

export default Register;
