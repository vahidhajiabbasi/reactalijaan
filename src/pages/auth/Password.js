import React, {useState} from 'react';
import {Grid, TextField, Button, FormLabel} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import useStyles from "./Style";
import FingerprintJS from "@fingerprintjs/fingerprintjs";
import {Post} from "../../services/api";

const Password = () => {

    const classes = useStyles();

    const[password, setPassword] = useState();
    const [codeError, setCodeError] = useState('');

    let history = useHistory();

    const handlePasswordInputChange = (sender) => {
        setPassword(sender.target.value);
    };

    const handleError = () => {
        setCodeError("رمز عبور وارد شده اشتباه است");
    };

    const Login = async() => {
        const fp = await FingerprintJS.load();
        const fingerPrint = await fp.get();
        const data = await Post(
            'user/auth/login',
            {Email: localStorage.getItem('loginInput'), Password:password, FingerPrint:fingerPrint.visitorId, FcmToken:null},
            handleError
        );
        localStorage.setItem('token', data.Data.token);
        history.push('/')
    };

    return (
        <Grid container direction="column" alignItems="center">
            <Button onClick={()=>{history.goBack()}}>back</Button>
            <img src={"/logo192.png"} alt="logo" className={classes.logo}/>
            <h1>رمز عبور</h1>
            <FormLabel>رمز عبور خود را وارد نمایید</FormLabel>
            <TextField
                variant="outlined"
                className="text-field"
                type="password"
                fullWidth={true}
                error={!!codeError}
                helperText={codeError}
                onChange={(e)=>handlePasswordInputChange(e)}
            />
            <Button
                variant="contained"
                color="primary"
                fullWidth={true}
                onClick={()=>Login()}
            >
                ادامه
            </Button>
        </Grid>
    );
};

export default Password;
