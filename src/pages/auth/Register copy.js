import React, {useState} from 'react';
import {Grid, TextField, Button, FormLabel} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import {validateEmail, validateMobile} from '../../utils/Validator'
import useStyles from './Style';
import { Post } from "../../services/api";

const Register = () => {

    const classes = useStyles();

    let history = useHistory();

    const [input, setInput] = useState(localStorage.getItem('loginInput') || '');
    const [inputError, setInputError] = useState('');
    const [loading, setLoading] = useState(false);
    const [btnLabel, setBtnLabel] = useState('ورود');

    const login = () => {
        if(validateEmail(input)){
            localStorage.setItem('loginInput', input);
            history.push('/user/login/password')
        } else if (validateMobile(input)){
            loginByMobile()
        } else {
            setInputError('ایمیل یا شماره موبایل نا معتبر است')
        }
    };

    const changeBtnLabel = () => {
        setLoading(false);
        setBtnLabel('تلاش مجدد');
    };

    const loginByMobile = async ()=> {
        localStorage.setItem('loginInput', input);
        setLoading(true);
        await Post(
            'user/auth/registerMobile',
            {Mobile: input}
        );
        history.push('/user/login/confirm')
    };

    const handleInputChange = (sender) => {
        setInput(sender.target.value);
    };

    return (
        <Grid container direction="column" alignItems="center">
            <img src={"/logo192.png"} alt="logo" className={classes.logo}/>
            <h1>ورود</h1>
            <FormLabel>شماره موبایل یا پست الکترونیک خود را وارد کنید</FormLabel>
            <TextField
                variant="outlined"
                className="text-field"
                onChange={(e)=>handleInputChange(e)}
                value={input}
                fullWidth={true}
                error={!!inputError}
                helperText={inputError}
            />
            <Button
                variant="contained"
                color="primary"
                fullWidth={true}
                disabled={loading}
                onClick={()=> login()}
            >
                {btnLabel}
            </Button>
            <small>یا ورود یا</small>
            <Button variant="contained" color="secondary">G-mail</Button>
        </Grid>
    );
};

export default Register;
