import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
    logo:{
        height: "100px",
        width: "100px",
        marginBottom: "15px",
    },
});

export default useStyles;
