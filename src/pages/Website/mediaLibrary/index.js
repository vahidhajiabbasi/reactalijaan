import React, {useEffect, useState} from 'react';
import PageTitle from "../../../components/mainLayout/PageTitle";
import useStyles from './Style';
import MediaFilterBar from "../../../components/mediaLibrary/MediaFilterBar";
import {Button, Grid} from "@material-ui/core";
import {Get} from "../../../services/api";
import Uploader from "../../../components/uplodaer";
import {ImageSize} from "../../../utils/ImageSize";
import MediaInfoDialog from "../../../components/mediaLibrary/mediaInfoDialog";
import DataDialog from "../../../components/dialog";
import {IMAGES_BASE_URL} from "../../../config";

const MediaLibrary = () => {
    const classes = useStyles();
    const [showUploader, setShowUploader] = useState(false);
    const [selectedMediaId, setSelectedMediaId] = useState(null);
    const [allFiles, setAllFiles] = useState([]);
    const [page, setPage] = useState(1)

    useEffect(()=>{
        getFilesFromServer();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [page])

    const getFilesFromServer = async () => {
        const files = await Get(`file/all?Page=${page}&PerPage=18&OrderBy=id&Order=DESC`);
        files && setAllFiles(prevState => {
            return [...prevState , ...files.Data.Files]
        });
    }

    const handleDeleteMedia = (mediaId) => {
        setSelectedMediaId(false);
        const fileIndex = allFiles.findIndex(file=>file.Id === mediaId);
        let allFileCopy = [...allFiles];
        allFileCopy.splice(fileIndex, 1);
        setAllFiles(allFileCopy);
    }

    return (
        <>
            <PageTitle title="رسانه ها" showHomeLink={true}/>
            <MediaFilterBar
                addNewFile={()=>setShowUploader(true)}
            />
            {
                <DataDialog
                    visibility={!!selectedMediaId}
                    onClose={()=>setSelectedMediaId(null)}
                >
                    <MediaInfoDialog
                        imageId={selectedMediaId}
                        onDelete={handleDeleteMedia}
                    />
                </DataDialog>
            }
            {showUploader && <Uploader
                title={"بارگذاری فایل"}
                onClose={()=>setShowUploader(false)}
                onUpload={()=>setShowUploader(false)}
            />
            }
            <div className={classes.postsContainer}>
                <Grid container spacing={3} >
                    {
                        allFiles.map(file=>{
                            return <Grid key={file.Id} item xs={12} sm={4} md={2} lg={2}>
                                <Button
                                    onClick={()=>setSelectedMediaId(file.Id)}
                                    fullWidth
                                >
                                    <img
                                        style={{height:"100%", width:"100%"}}
                                        src={ImageSize(IMAGES_BASE_URL+file.Url, 'thumb')}
                                        alt={file.Id}
                                    />
                                </Button>
                            </Grid>
                        })
                    }
                </Grid>
                <Button
                    onClick={()=>setPage(prevState => prevState+1)}
                >
                    load more
                </Button>
            </div>
        </>
    );
}

export default MediaLibrary;
