import React, {useEffect, useState} from 'react';
import PageTitle from "../../../components/mainLayout/PageTitle";
import useStyles from './Style';
import {
    Button,
    FormControl,
    Grid,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    TextField
} from "@material-ui/core";
import {Get, Post, Put} from "../../../services/api";
import IconButton from "@material-ui/core/IconButton";
import Icon from "../../../components/icon";

const PostTag = () => {
    const classes = useStyles();
    const [tags, setTags] = useState([]);
    const [tag, setTag] = useState({});

    useEffect(()=>{
        const getTags = async () => {
            const allTags = await Get('post/tag/all?Page=1&PerPage=10&OrderBy=Id&Order=DESC');
            setTags(allTags.Data.Rows);
        }
        getTags();
    }, []);

    const handleInputChange = (sender) => {
        let newInput = {};
        newInput[sender.target.id] = sender.target.value;
        setTag(prevState => {
            return {...prevState, ...newInput}
        })
    }

    const handleSelectTag = (id) => {
        const selectedTag = tags.find(tag=>tag.Id === id);
        setTag(selectedTag);
    }

    const saveTag = async () => {
        await Post("post/tag", tag);
        setTags(prevState => {
            return [...prevState, tag];
        })
    }

    const updateTag = async () => {
        await Put(`post/tag/${tag.Id}`, {
            Title: tag.Title,
            Description: tag.Description
        });
    }

    return (
        <>
            <PageTitle title="برچسب ها" showHomeLink={true}/>
            <Grid spacing={3} container>
                <Grid item md={6}>
                    <div className={classes.container}>
                        {tags.map(tag => <List>
                            <ListItem button onClick={()=>handleSelectTag(tag.Id)} >
                                <ListItemText
                                    primary={tag.Title}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete">
                                        <Icon name="delete"/>
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        </List>)}
                    </div>

                </Grid>
                <Grid item md={6} >
                    <div className={classes.container}>
                        <h3 style={{marginBottom:15}}>{tag.Id ? `ویرایش برچسب ${tag.Title}` : 'افزودن برچسب جدید'}</h3>
                        <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                            <TextField
                                label="عنوان"
                                variant="outlined"
                                id="Title"
                                value={tag.Title || ""}
                                onChange={handleInputChange}
                            />
                        </FormControl>
                        <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                            <TextField
                                label="پیوند یکتا"
                                variant="outlined"
                                id="Slug"
                                value={tag.Slug || ""}
                                onChange={handleInputChange}
                            />
                        </FormControl>
                        <FormControl variant="outlined" style={{marginBottom:20, marginTop:20}} fullWidth>
                            <textarea
                                placeholder="توضیحات"
                                id="Description"
                                value={tag.Description || ""}
                                onChange={handleInputChange}
                            />
                        </FormControl>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={tag.Id ? updateTag : saveTag}
                        >
                            ذخیره
                        </Button>
                    </div>
                </Grid>
            </Grid>
        </>
    );
}

export default PostTag;
