import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        container:{
            display:"flex",
            flexDirection:"column",
            padding:"20px !important",
            background:theme.colors.white,
            boxShadow: `0 0 10px ${theme.colors.mediumGray}`,
            borderRadius:"5px",
            marginTop:15,
            flex:"1 auto"
        },
    })
);

export default useStyles;
