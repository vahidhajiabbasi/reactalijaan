import React, {useEffect, useState} from 'react';
import PageTitle from "../../../components/mainLayout/PageTitle";
import PostsFilterBar from "../../../components/post/filterBar";
import PostCard from "../../../components/post/PostCard";
import useStyles from './Style';
import {Grid} from "@material-ui/core";
import {Get} from "../../../services/api";

const Posts = () => {
    const classes = useStyles();
    const [posts, setPosts] = useState([]);
    useEffect(()=>{
        async function set(){
            const allPosts = await Get('post/all?Page=1&PerPage=10&OrderBy=Id&Order=DESC')
            setPosts(allPosts.Data.Rows)
        }
        set();
    }, [])

    const handleDeletePost = (id) => {
        const postIndex = posts.findIndex(post=>post.Id === id);
        let newPosts = [...posts];
        newPosts.splice(postIndex, 1);
        setPosts(newPosts);
    }

    return (
        <>
            <PageTitle title="نوشته ها" showHomeLink={true}/>
            <PostsFilterBar/>
            <div className={classes.postsContainer}>
                <Grid container spacing={3} >
                    {
                        posts ?
                            posts.map(post=>{
                                return <Grid key={post.Id} item xs={12} sm={6} md={4} lg={3}>
                                    <PostCard post={post} onDelete={handleDeletePost}/>
                                </Grid>
                            }) : ""
                    }
                </Grid>
            </div>
        </>
    );
}

export default Posts;
