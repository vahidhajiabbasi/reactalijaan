import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        postsContainer:{
            flex:1,
            marginTop:"20px",
            overflowY:"scroll",
            overflowX:"hidden",
        },
    })
);

export default useStyles;
