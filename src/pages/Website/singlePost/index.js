import React, {useEffect, useState} from 'react';
import useStyles from './Style';
import {Get, Post, Put} from "../../../services/api";
import PageTitle from "../../../components/mainLayout/PageTitle";
import PageBuilder from "../../../components/pageBuilder";
import {
    Accordion,
    AccordionDetails,
    AccordionSummary, Button, FormControl, Grid, InputLabel, Select
} from "@material-ui/core";
import { useTheme } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconBox from "../../../components/IconBox";
import Icon from "../../../components/icon";
import {v4 as Uuid} from "uuid";
import PostTitle from "../../../components/post/singlePost/PostTitle";
import PostExcerpt from "../../../components/post/singlePost/PostExcerpt";
import PostCategory from "../../../components/post/singlePost/PostCategory";
import PostTag from "../../../components/post/singlePost/PostTag";
import PostSeo from "../../../components/post/singlePost/PostSeo";
import Uploader from "../../../components/uplodaer";
import {IMAGES_BASE_URL} from "../../../config";
import {ImageSize} from "../../../utils/ImageSize";
import MediaSelectionDialog from "../../../components/mediaLibrary/mediaSelectionDialog";
import DataDialog from "../../../components/dialog";

const SinglePost = ({ match }) => {
    const classes = useStyles();
    const theme = useTheme();

    const [sections, setSections] = useState([]);
    const [showImageSelector, setShowImageSelector] = useState(false);
    const [post, setPost] = useState({
        ParentId: null,
        Image: null,
        Title: null,
        Excerpt: null,
        Content: [],
        Seo: '{"title":"بهینه سازی","description":"سئو ممکن است انواع مختلف جستجو را دربرگیرد؛"}',
        tags: [],
        categories: [],
        image:{}
    });

    //get post id from uri
    const postId = match.params.id;

    /**
     * get post details from server
     */
    useEffect(()=>{
        async function set(){
            // declare free response
            let postDetail = {Data:{Content:[]}};

            //if is post id call Api
            if(postId !== undefined){
                postDetail = await Get(`post/${postId}`)
                setPost(postDetail.Data)
            }

            //set accordion sections
            setSections([
                {title:"عنوان", icon:"header", component:<PostTitle
                        title={postDetail.Data.Title}
                        slug={postDetail.Data.Slug}
                        onChange={handlePostChange}
                    />
                },
                {title:"محتوا", icon:"paragraph", component:<PageBuilder
                        contents={postDetail.Data.Content}
                        onChange={handlePostChange}
                    />
                },
                {title:"چکیده", icon:"summary", component:<PostExcerpt
                        text={postDetail.Data.Excerpt}
                        onChange={handlePostChange}
                    />
                },
                {title:"دسته بندی", icon:"catategory", component:<PostCategory
                        categories={postDetail.Data.categories}
                        onChange={handlePostChange}
                    />},
                {title:"برچسب", icon:"tag", component:<PostTag
                        tags={postDetail.Data.tags}
                        onChange={handlePostChange}
                    />},
                {title:"تنظیمات سئو", icon:"seo", component:<PostSeo
                        seoJson={postDetail.Data.Seo}
                        onChange={handlePostChange}
                    />},
            ]);
        }
        set();
    }, [postId])

    /**
     * handle post components changes
     * @param changes
     */
    const handlePostChange = (changes) => {
        setPost(prevState => {
            return {...prevState, ...changes}
        })
    }

    /**
     * handle post image change
     * @param image
     */
    const handleImageChange = (image) => {
        let newImage = post.image;
        newImage.Url = image.Path;
        newImage.Id = image.Id;
        setPost(prevState => {
            return {...prevState, image:newImage, Image:image.Id}
        });
        setShowImageSelector(false);
    }

    /**
     *
     * @returns {Promise<void>}
     */
    const updatePost = async () => {
        let updatableData = {
            ParentId: null,
            Image: post.Image,
            Title: post.Title,
            Excerpt: post.Excerpt,
            Content: post.Content,
            Seo: post.Seo,
            Tags: post.Tags,
            categories: post.Categories
        }
        await Put(`post/${postId}`, updatableData);
    }

    const savePost = async () => {
        let saveData = {
            ParentId: null,
            Image: post.Image,
            Title: post.Title,
            Slug:Uuid(),
            Excerpt: post.Excerpt,
            Content: post.Content,
            Seo: post.Seo,
            Tags: post.Tags,
            categories: post.Categories
        }
        const postDetail = await Post(`post`, saveData)
        window.history.replaceState(null, "", `/website/post/${postDetail.Data.PostId}`)
    }

    return (
        <>
            <PageTitle
                title={post.Title ? `ویرایش نوشته ${post.Title}`: "افزودن نوشته جدید"}
                showHomeLink={true}
            />
            <DataDialog visibility={showImageSelector} onClose={()=>setShowImageSelector(false)}>
                <MediaSelectionDialog onSelect={handleImageChange}/>
            </DataDialog>
            <Grid spacing={3} container className={classes.container}>
                <Grid item md={9} sm={12} className={classes.postsContainer}>
                    {
                        sections.map(section => {
                            return <div style={{marginBottom:10}} key={section.title}>
                                <Accordion>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                        aria-controls="panel1c-content"
                                        id="panel1c-header"
                                    >
                                        <IconBox height={30} width={30} padding={10}>
                                            <Icon name={section.icon} size={30} color={theme.colors.blue}/>
                                        </IconBox>
                                        <h2>{section.title}</h2>
                                    </AccordionSummary>
                                    <AccordionDetails className={classes.accordionDetails}>
                                        {section.component}
                                    </AccordionDetails>
                                </Accordion>
                            </div>
                        })
                    }
                </Grid>
                <Grid item md={3} sm={12} className={classes.postControls}>
                    <Button
                        style={{marginBottom:15}}
                        variant="contained"
                        color="primary"
                        fullWidth
                        onClick={post.Id ? updatePost : savePost}
                    >
                        {post.Id ? "ذخیره تغییرات" : "انتشار نوشته"}
                    </Button>
                    <Button
                        style={{marginBottom:15}}
                        variant="outlined"
                        color="primary"
                        fullWidth
                    >
                        پیش نویس
                    </Button>
                    <FormControl
                        style={{marginBottom:15}}
                        variant="outlined"
                        fullWidth
                        size={"small"}
                        className={classes.formControl}
                    >
                        <InputLabel htmlFor="outlined-age-native-simple">قالب نوشته</InputLabel>
                        <Select
                            native
                            label="قالب نوشته"
                        >
                            <option aria-label="None" value="" />
                            <option value={0}>محتوا</option>
                            <option value={1}>گالری تصویر</option>
                            <option value={1}>ویدئو</option>
                        </Select>
                    </FormControl>

                    {post.image ?
                        <>
                            <div className={classes.postImage} onClick={()=>setShowImageSelector(true)}>
                                <img
                                    src={ImageSize(IMAGES_BASE_URL+post.image.Url, 'thumb')}
                                    alt={post.image.Alt}
                                />
                            </div>
                            <Button
                                style={{marginTop:15}}
                                variant="outlined"
                                color="primary"
                                fullWidth
                                onClick={()=>setShowImageSelector(true)}
                            >
                               تغییر تصویر نوشته
                            </Button>
                        </>
                        :
                        <Uploader />
                    }
                </Grid>
            </Grid>

        </>
    );
}

export default SinglePost;
