import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            display:"flex"
        },
        postsContainer:{
            flex:1,
            marginTop:"20px",
            paddingLeft:10,
            marginLeft:10,
            overflowY:"auto",
            maxHeight:"calc(100vh - 182px)",
        },
        accordionDetails:{
            flexDirection:"column",
        },
        container:{
            height:"calc(100vh - 182px)",
            overflowY:"hidden"
        },
        postControls:{
            borderRadius:4,
            padding:40,
            minWidth:200,
            marginTop:"20px",
            background:theme.colors.white
        },
        postImage:{
            overflow:"hidden",
            display: "flex",
            width:"100%",
            "& img":{
                width:"100%",
                height:"auto",
            }
        }
    })
);

export default useStyles;
