import React, {useEffect, useState} from 'react';
import PageTitle from "../../../components/mainLayout/PageTitle";
import useStyles from './Style';
import {
    Avatar,
    Button,
    FormControl,
    Grid, InputLabel,
    List,
    ListItem,
    ListItemAvatar, ListItemSecondaryAction,
    ListItemText, Select,
    TextField
} from "@material-ui/core";
import {Get, Post, Put} from "../../../services/api";
import IconButton from "@material-ui/core/IconButton";
import Icon from "../../../components/icon";
import Base64Uploader from "../../../components/Base64Uploader";

const PostCategory = () => {
    const classes = useStyles();
    const [categories, setCategories] = useState([]);
    const [category, setCategory] = useState({ParentId:null});

    useEffect(()=>{
        const getCategories = async () => {
            const allCategories = await Get('post/category/all?Page=1&PerPage=10&OrderBy=Id&Order=DESC');
            setCategories(allCategories.Data.Rows);
        }
        getCategories();
    }, []);

    const handleIconChange = (icon) => {
        setCategory(prevState => {
            return {...prevState, Icon:icon}
        })
    }

    const handleInputChange = (sender) => {
        let newInput = {};
        newInput[sender.target.id] = sender.target.value;
        setCategory(prevState => {
            return {...prevState, ...newInput}
        })
    }

    const handleSelectCategory = (id) => {
        const selectedCategory = categories.find(category=>category.Id === id);
        setCategory(selectedCategory);
    }

    const saveCategory = async () => {
        await Post("post/category", category);
        setCategories(prevState => {
            return [...prevState, category];
        })
    }

    const updateCategory = async () => {
        await Put(`post/category/${category.Id}`, {
            ParentId: category.ParentId,
            Title: category.Title,
            Icon: category.Icon,
            Description: category.Description
        });
        // setCategories(prevState => {
        //     return [...prevState, category];
        // })
    }

    return (
        <>
            <PageTitle title="دسته بندی ها" showHomeLink={true}/>
            <Grid spacing={3} container>
                <Grid item md={6}>
                    <div className={classes.container}>
                        {categories.map(category => <List>
                            <ListItem button onClick={()=>handleSelectCategory(category.Id)} >
                                <ListItemAvatar>
                                    <Avatar >
                                        { category.Icon && <img
                                            style={{width:"60%"}}
                                            src={category.Icon}
                                            alt={category.Title}
                                        />}
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={category.Title}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete">
                                        <Icon name="delete"/>
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        </List>)}
                    </div>

                </Grid>
                <Grid item md={6} >
                    <div className={classes.container}>
                        <h3 style={{marginBottom:15}}>{category.Id ? `ویرایش دسته بندی ${category.Title}` : 'افزودن دسته بندی جدید'}</h3>
                        <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                            <TextField
                                label="عنوان"
                                variant="outlined"
                                id="Title"
                                value={category.Title || ""}
                                onChange={handleInputChange}
                            />
                        </FormControl>
                        <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                            <InputLabel htmlFor="outlined-age-native-simple">دسته بندی پدر</InputLabel>
                            <Select
                                native
                                label="دسته بندی پدر"
                                id="ParentId"
                                onChange={handleInputChange}
                            >
                                <option aria-label="None" value="" />
                                {categories.map(item =><option
                                    value={item.Id}
                                    selected={item.Id === category.ParentId}
                                >{item.Title}</option>)}
                            </Select>
                        </FormControl>
                        <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                            <TextField
                                label="پیوند یکتا"
                                variant="outlined"
                                id="Slug"
                                value={category.Slug || ""}
                                onChange={handleInputChange}
                            />
                        </FormControl>
                        <Base64Uploader
                            title="انتخاب آیکون (فقط svg)"
                            onUpload={handleIconChange}
                            format="svg"
                            base64={category.Icon}
                        />
                        <FormControl variant="outlined" style={{marginBottom:20, marginTop:20}} fullWidth>
                            <textarea
                                placeholder="توضیحات"
                                id="Description"
                                value={category.Description || ""}
                                onChange={handleInputChange}
                            />
                        </FormControl>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={category.Id ? updateCategory : saveCategory}
                        >
                            ذخیره
                        </Button>
                    </div>
                </Grid>
            </Grid>
        </>
    );
}

export default PostCategory;
