import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            height:"100vh",
            background:theme.colors.backgroundGray
        },
        app:{
            display:"flex"
        },
        contents:{
            position:"relative",
            display:"flex",
            flexGrow: 1,
            flexFlow: "column",
            height: "calc(100vh - 160px)",
            padding: theme.spacing(3),
        }
    })
);

export default useStyles;
