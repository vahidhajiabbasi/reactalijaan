import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
        }
    })
);

export default useStyles;
