import React from 'react';
import { useHistory } from 'react-router-dom';
import Button from "@material-ui/core/Button";
import useStyles from './Style';

const PageTitle = ({title, showHomeLink}) => {

    const history = useHistory();
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <h1>{title}</h1>
            {
                showHomeLink ?
                    <Button
                        onClick={()=>history.push('/')}
                    >
                        پیشخوان اصلی
                    </Button> : ''
            }
        </div>
    );
};

export default PageTitle;
