import React from 'react';
import useStyles from './Style'
import { Toolbar } from '@material-ui/core';

const Footer = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Toolbar className={classes.footerContainer}>
                <p>تمامی حقوق مادی و معنوی برای لیدو بات محفوظ است.</p>
            </Toolbar>
        </div>
    );
};

export default Footer;
