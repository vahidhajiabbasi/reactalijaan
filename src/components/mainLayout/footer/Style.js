import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            position:"fixed",
            width:'100%',
            bottom: 0,
            backgroundColor: "#fff",
            boxShadow: "0 0 10px lightGray"
        },
        footerContainer:{
            minHeight:"50px!important"
        }
    })
);

export default useStyles;
