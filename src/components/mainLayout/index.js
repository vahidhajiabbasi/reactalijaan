import React from 'react';
import Header from "./header/Header";
import Sidebar from "./sidebar";
import useStyles from './Style';
import Footer from "./footer";

const MainLayout = ({children}) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Header />
            <div className={classes.app}>
                <Sidebar/>
                <main className={classes.contents}>
                    {children}
                </main>
            </div>
            <Footer/>
        </div>
    );
};

export default MainLayout;
