import { makeStyles } from "@material-ui/core/styles";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
        drawer: {
            width: drawerWidth+20,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
            height: "calc(100% - 154px)",
            top:84,
            right:20,
            left:"auto",
            boxShadow: `0 0 10px ${theme.colors.mediumGray}`,
            borderRadius:"5px",
            border:"none",
        },
        drawerContainer: {
            overflow: 'auto',
        },
        list:{
            padding:0,
            "& span": {
                textAlign: "right",
                lineHeight: '30px',
                color:theme.colors.darkBlue
            }
        },
        listItem:{
            padding:"10px 30px 10px 10px"
        },
        active:{
            padding:"10px 25px 10px 10px",
            background: `${theme.colors.lightGray} !important `,
            borderRight: `5px solid ${theme.colors.blue}`,
            "& span": {
                fontFamily: "iranSansBold!important"
            }
        },
    })
);

export default useStyles;
