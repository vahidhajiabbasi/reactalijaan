import React, {useEffect, useState} from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import useStyles from './Style';
import {Get} from "../../../services/api";
import { Menus } from "../../../constants/Menus"
import Theme from "../../../globalStyles/theme";
import Icon from "../../icon";

export default function Sidebar() {

    const classes = useStyles();
    const history = useHistory();

    const [menuList, setMenuList] = useState([]);

    const location = useLocation();
    const path = location.pathname;

    const basePath = '/'+location.pathname.split('/')[1];

    useEffect(()=>{
        getMenuList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getMenuList = async () => {
        const cachedMenu = sessionStorage.getItem(basePath);
        if(cachedMenu){
            setMenuList(JSON.parse(cachedMenu));
            return;
        }
        //get all user permissions
        const userPermissions = await Get('user/permission/all');
        const permissionNames = userPermissions.Data.map(permission => permission.Name);

        if(! Menus[basePath]) return;

        // eslint-disable-next-line array-callback-return
        const menus = Menus[basePath].map((menu) => {
            const intersection = permissionNames.filter(element => menu.permissions.includes(element));
            if(intersection){
                return {title: menu.title, path:menu.path, icon:menu.icon};
            }
        });
        sessionStorage.setItem(basePath, JSON.stringify(menus));
        setMenuList(menus);
    };

    return (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.drawerContainer}>
                <List className={classes.list}>
                    <ListItem
                        button
                        className={classes.listItem}
                        selected={path === "/"}
                        classes={{ selected: classes.active }}
                        onClick={()=>history.push("/")}
                        key={0}
                    >
                        <Icon name="home" size={28} color={Theme.colors.blue}/>
                        <ListItemText primary="پیشخوان" />
                    </ListItem>
                    {
                        menuList.map((menu, index) => {
                            return(
                                <ListItem
                                    button
                                    className={classes.listItem}
                                    selected={path === menu.path}
                                    classes={{ selected: classes.active }}
                                    onClick={()=>history.push(menu.path)}
                                    key={index+1}
                                >
                                    <Icon name={menu.icon} size={28} color={Theme.colors.blue}/>
                                    <ListItemText primary={menu.title} />
                                </ListItem>
                            )
                        })
                    }
                </List>
            </div>
        </Drawer>
    );
}
