import React, {useEffect, useState} from 'react';
import useStyles from "./Style";
import {
    Dialog,
    DialogContent,
    DialogContentText,
    DialogTitle, IconButton,
    useTheme
} from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Icon from "../icon";

const DataDialog = ({title, description, children, visibility, onClose }) => {
    const classes = useStyles();
    const theme = useTheme();
    const screenQuery = useMediaQuery(theme.breakpoints.down('sm'));
    const [dialogVisibility, setDialogVisibility] = useState(false);

    useEffect(()=>{
        setDialogVisibility(visibility)
    }, [visibility])

    const handleClose = () => {
        setDialogVisibility(false);
        onClose();
    }
    return (
        <Dialog
            fullWidth={true}
            maxWidth={"md"}
            open={dialogVisibility}
            onClose={handleClose}
            fullScreen={screenQuery}
        >
            <IconButton
                className={classes.closeBtn}
                variant={"contained"}
                color="default"
                onClick={handleClose}
            >
                <Icon name="close" size={20}/>
            </IconButton>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                {description && <DialogContentText>{description}</DialogContentText>}
                {
                   children
                }
            </DialogContent>
        </Dialog>
    );
};

export default DataDialog;