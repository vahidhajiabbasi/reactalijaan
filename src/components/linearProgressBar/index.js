import React from 'react';
import {Box, LinearProgress} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

function LinearProgressBar(props) {
    return (
        <Box display="flex" alignItems="center" mb={1}>
            <Box width="100%" ml={1}>
                <LinearProgress variant="determinate" value={props.value > 100 ? 100 : Math.round(props.value,)} />
            </Box>
            <Box minWidth={35}>
                <Typography variant="body2" color="textSecondary" align="left">
                    {`${props.value > 100 ? 100 : Math.round(props.value,)}%`}
                </Typography>
            </Box>
        </Box>
    );
}

export default LinearProgressBar