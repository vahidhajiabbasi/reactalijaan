import React from 'react';
import { Grid, Card, CardContent } from '@material-ui/core';
import useStyles from './Style';

const AuthLayout = ({children}) => {
    const classes = useStyles();
    return (
        <Grid container justify="center" alignItems="center" className={classes.root}>
            <Card className={classes.card}>
                <CardContent>
                    {children}
                </CardContent>
            </Card>
        </Grid>
    );
};

export default AuthLayout;
