import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            height:"100vh",
            background: theme.colors.backgroundGray
        },
        card:{
            boxShadow: "0 0 10px $lightGray!important",
            borderRadius: "5px!important",
            width: "400px",
            padding: "20px",
            "& *":{
                textAlign: "center"
            },
            "& label":{
                fontSize: "0.9rem",
                margin:"10px 0"
            },
            "& small":{
                fontSize: "0.9rem",
                margin:"15px 0"
            },
        },
    })
);

export default useStyles;
