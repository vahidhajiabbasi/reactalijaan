import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            flex:"0 auto",
            width:"calc(100% - 30px)",
            padding: '15px',
            marginTop:20,
            background:theme.colors.white,
            boxShadow: `0 0 10px ${theme.colors.mediumGray}`,
            borderRadius:"5px",
            display:"flex",
            justifyContent:"space-between"
        },
        formControlContainer:{
            width:"calc(100% - 160px)"
        },
        formControl:{
            width:"100%"
        },
        squareBtn:{
            width:40,
            height:40,
            padding:0,
            background: theme.colors.lightGray,
            minWidth:30,
        },
        mediaInfo:{
            display:"flex",
            flexDirection:"row-reverse",
            justifyContent:"space-between"
        },
        mediaInfoImg:{
            width:"50%",
            padding:20,
            display: "flex",
            overflow:"hidden",
            border:`1px solid ${theme.colors.gray}`,
            borderRadius:3,
            '&::before':{
                content: '""',
                float: "left",
                paddingTop: "100%"
            }
        },
        mediaSelection:{
            flexGrow: 1,
            backgroundColor: theme.palette.background.paper,
            display: 'flex',
            minHeight:320
        },
        mediaSelectionTabs: {
            borderLeft: `1px solid ${theme.palette.divider}`,
        },
    })
);

export default useStyles;
