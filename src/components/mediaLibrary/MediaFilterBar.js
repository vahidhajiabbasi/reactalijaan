import React from 'react';
import {Grid, Button, Select, FormControl, InputLabel} from "@material-ui/core";
import useStyles from './Style';
import TextField from "@material-ui/core/TextField";
import {Add} from '../svgIcon';
import Theme from '../../globalStyles/theme';

const MediaFilterBar = ({addNewFile}) => {

    const classes = useStyles();

    const colWidth = 3;

    return (
        <div className={classes.root}>
            <Button
                className={classes.squareBtn}
                onClick={addNewFile}
            >
                <Add fill={Theme.colors.blue} width={20} height={20}/>
            </Button>
            <Grid container spacing={3} className={classes.formControlContainer}>
                <Grid item xs={12} sm={colWidth}>
                    <FormControl variant="outlined" size={"small"} className={classes.formControl}>
                        <InputLabel htmlFor="outlined-age-native-simple">نوع رسانه</InputLabel>
                        <Select
                            native
                            label="نوع رسانه"
                            inputProps={{
                                name: 'age',
                                id: 'outlined-age-native-simple',
                            }}
                        >
                            <option aria-label="None" value="" />
                            <option value={0}>عکس</option>
                            <option value={1}>ویدئو</option>
                            <option value={1}>پی دی اف</option>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={colWidth}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <TextField
                            size={"small"}
                            id="outlined-basic"
                            label="ازتاریخ"
                            variant="outlined"
                        />
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={colWidth}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <TextField
                            size={"small"}
                            id="outlined-basic"
                            label="تا تاریخ"
                            variant="outlined"
                        />
                    </FormControl>
                </Grid>
            </Grid>
            <Button className={classes.squareBtn}>
                <Add fill={Theme.colors.blue} width={20} height={20}/>
            </Button>
            <Button className={classes.squareBtn}>
                <Add fill={Theme.colors.blue} width={20} height={20}/>
            </Button>
        </div>
    );
};

export default MediaFilterBar;
