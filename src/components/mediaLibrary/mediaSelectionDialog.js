import React, {useEffect, useState} from 'react';
import useStyles from './Style';
import {Box, Button, Grid, Tab, Tabs} from "@material-ui/core";
import Uploader from "../uplodaer";
import {Get} from "../../services/api";
import {ImageSize} from "../../utils/ImageSize";
import {IMAGES_BASE_URL} from "../../config";

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

const MediaSelectionDialog = ({onSelect}) => {
    const classes = useStyles();
    const [value, setValue] = useState(0);

    const [allFiles, setAllFiles] = useState([]);

    useEffect(()=>{
        const set = async () => {
            const files = await Get("file/all?Page=1&PerPage=10&OrderBy=id&Order=DESC");
            files && setAllFiles(files.Data.Files);
        }
        set();
    }, [])

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.mediaSelection}>
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={value}
                onChange={handleChange}
                aria-label="Vertical tabs example"
                className={classes.mediaSelectionTabs}
            >
                <Tab label="بارگذاری رسانه جدید" {...a11yProps(0)} />
                <Tab label="انتخاب از کتابخانه" {...a11yProps(1)} />
            </Tabs>
            <TabPanel value={value} index={0}>
                <Uploader title={'بارگذاری رسانه جدید'} onUpload={(file)=>onSelect(file)}/>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <div>
                    <Grid container spacing={3} >
                        {
                            allFiles.map(file=>{
                                return <Grid item xs={12} sm={4} md={2} lg={2}>
                                    <Button
                                        onClick={()=>onSelect({Path:file.Url, Id:file.Id})}
                                        fullWidth
                                    >
                                        <img
                                            key={file.Id}
                                            style={{height:"100%", width:"100%"}}
                                            src={ImageSize(IMAGES_BASE_URL+file.Url, 'thumb')}
                                            alt={file.Id}
                                        />
                                    </Button>
                                </Grid>
                            })
                        }
                    </Grid>
                </div>
            </TabPanel>
        </div>
    )
};

export default MediaSelectionDialog;
