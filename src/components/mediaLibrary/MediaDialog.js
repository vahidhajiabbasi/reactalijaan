import React, {useEffect, useState} from 'react';
import {Get} from "../../services/api";
import {Button, Grid} from "@material-ui/core";
import {ImageSize} from "../../utils/ImageSize";
import {IMAGES_BASE_URL} from "../../config";

const MediaDialog = ({onSelect}) => {

    const [allFiles, setAllFiles] = useState([]);

    useEffect(()=>{
        const set = async () => {
            const files = await Get("file/all?Page=1&PerPage=10&OrderBy=id&Order=DESC");
            files && setAllFiles(files.Data.Files);
        }
        set();
    }, [])

    return (
        <div>
            <Grid container spacing={3} >
                {
                    allFiles.map(file=>{
                        return <Grid item xs={12} sm={4} md={2} lg={2}>
                            <Button
                                onClick={()=>onSelect({Path:file.Url, Id:file.Id})}
                                fullWidth
                            >
                                <img
                                    key={file.Id}
                                    style={{height:"100%", width:"100%"}}
                                    src={ImageSize(IMAGES_BASE_URL+file.Url, 'thumb')}
                                    alt={file.Id}
                                />
                            </Button>
                        </Grid>
                    })
                }
            </Grid>
        </div>
    );
};

export default MediaDialog;