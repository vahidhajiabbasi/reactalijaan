import React, {useEffect, useState} from 'react';
import useStyles from './Style';
import {Delete, Get, Put} from "../../services/api";
import {Button, FormControl, TextField} from "@material-ui/core";

const MediaInfoDialog = ({imageId, onDelete}) => {
    const classes = useStyles();
    const [mediaDetail, setMediaDetail] = useState({});

    useEffect(()=>{
        const getImageDetail = async () => {
            const details = await Get(`file/${imageId}`);
            setMediaDetail(details.Data);
        }
        getImageDetail();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const updateMedia = async () => {
        await Put(
            `file/${mediaDetail.Id}`,
            {
                Alt:mediaDetail.Alt
            }
        )
    }

    const deleteMedia = async() => {
        await Delete(`file/${mediaDetail.Id}`);
        onDelete(mediaDetail.Id);
    }

    return (
            <>
                {mediaDetail.Url && <div className={classes.mediaInfo}>
                <div style={{paddingRight:20}}>
                    <div>
                        <p><strong>نام :</strong>{mediaDetail.Url.split('/').pop().split('.')[0]}</p>
                        <p><strong>نوع :</strong>{mediaDetail.MimeType}</p>
                        <p><strong>تاریخ بارگذاری :</strong>{mediaDetail.CreateDate}</p>
                        <p><strong>تاریخ آخرین آپدیت :</strong>{mediaDetail.UpdateDate}</p>
                        <p><strong>آدرس :</strong>{mediaDetail.Url}</p>
                    </div>
                    <div style={{marginTop:40}}>
                        <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                            <TextField
                                label="عنوان"
                                variant="outlined"
                                value={mediaDetail.Url.split('/').pop().split('.')[0] || ""}
                            />
                        </FormControl>
                        <FormControl variant="outlined" fullWidth>
                            <TextField
                                label="متن جایگزین"
                                variant="outlined"
                                value={mediaDetail.Alt || ""}
                                onChange={(e)=>setMediaDetail(prevState =>{
                                    return {...prevState, Alt:e.target.value}
                                })}
                            />
                        </FormControl>
                    </div>
                    <div style={{marginTop:40}}>
                        <Button
                            variant="contained"
                            color="primary"
                            style={{marginLeft:20}}
                            onClick={updateMedia}
                        >
                            ذخیره تغییرات
                        </Button>
                        <Button
                            variant="outlined"
                            color="secondary"
                            onClick={deleteMedia}
                        >
                            پاک کردن
                        </Button>
                    </div>
                </div>
                <div className={classes.mediaInfoImg}>
                    <img
                        style={{width:"100%", objectFit:"scale-down"}}
                        src={"http://127.0.0.1:5091/"+mediaDetail.Url}
                        alt={mediaDetail.id}
                    />
                </div>
                </div>
                }

            </>
    );
};

export default MediaInfoDialog;
