import React, {useEffect, useRef, useState} from 'react';
import useStyles from "./Style";
import {Button, IconButton, useTheme} from "@material-ui/core";
import Icon from "../icon";

const Base64Uploader = ({title, base64, format, closeBtn, onUpload, onClose}) => {
    const classes = useStyles();
    const theme = useTheme();
    const [file, setFile] = useState(null);

    const input = useRef(null)

    useEffect(()=>{
        setFile(base64);
    }, [base64])

    const browse = () => {
        input.current.click();
    }

    const onChange = (sender) => {
        const extension = sender.target.files[0].name.split('.').pop().toLowerCase();
        if(extension !== format) return;
        uploadImage(sender.target.files[0]);
    }

    const uploadImage = async (file) => {
        const reader = new FileReader();
        reader.onload = function(e) {
            setFile(e.target.result);
            onUpload(e.target.result);
        }
        reader.readAsDataURL(file);
    }

    return (
        <div>
            <div id="drop" className={classes.root}>
                {file ?
                    <>
                        <img src={file} alt="upload"/>
                    </>
                    :
                    <>
                        <Icon name="image" size={30} color={theme.colors.gray}/>
                        <span style={{color:theme.colors.gray}}>{title}</span>
                        <span style={{color:theme.colors.gray, fontSize:"0.8rem"}}>(فایل را بکشید و در این قسمت رها کنید)</span>
                        <span style={{color:theme.colors.gray}}>یا</span>
                        <Button
                            variant={"contained"}
                            color="default"
                            onClick={() => browse()}
                        >
                            انتخاب فایل
                        </Button>
                        <input type="file" name="upload" ref={input} onChange={(e) => onChange(e)}/>
                        {closeBtn && <IconButton
                            className={classes.closeBtn}
                            variant={"contained"}
                            color="default"
                            onClick={onClose}
                        >
                            <Icon name="close" size={20}/>
                        </IconButton> }
                    </>
                }
            </div>
        </div>
    );
};

export default Base64Uploader;