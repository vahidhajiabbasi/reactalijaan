import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            display: "flex",
            position: "relative",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            border:`2px dashed ${theme.colors.gray}`,
            borderRadius: 3,
            height: 200,
            overflow: "hidden",
            padding:20,
            "& span":{
                fontSize:16,
            },
            "& input":{
                display:"none"
            }
        },
        closeBtn:{
            position:"absolute",
            width:40,
            height:40,
            left:10,
            top:10,
        }
    })
);

export default useStyles;
