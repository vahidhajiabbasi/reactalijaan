import React, {useRef} from 'react';
import useStyles from "./Style";
import {Button, IconButton, useTheme} from "@material-ui/core";
import {Upload} from "../../services/api";
import Icon from "../icon";

const Uploader = ({title, closeBtn, onUpload, onClose}) => {
    const classes = useStyles();
    const theme = useTheme();

    const input = useRef(null)

    const browse = () => {
        input.current.click();
    }

    const onChange = (sender) => {
        uploadImage(sender.target.files[0]);
    }

    const uploadImage = async (image) => {
        let bodyFormData = new FormData();
        bodyFormData.append('file', image);
        bodyFormData.append('path', 'posts');
        const res = await Upload('file/upload', bodyFormData);
        res && onUpload(res.Data);
    }

    return (
        <div id="drop" className={classes.root}>
            <Icon name="image" size={30} color={theme.colors.gray}/>
            <span style={{color:theme.colors.gray}}>{title}</span>
            <span style={{color:theme.colors.gray, fontSize:"0.8rem"}}>(فایل را بکشید و در این قسمت رها کنید)</span>
            <span style={{color:theme.colors.gray}}>یا</span>
            <Button
                variant={"contained"}
                color="default"
                onClick={() => browse()}
            >
                انتخاب فایل
            </Button>
            <input type="file" name="upload" ref={input} onChange={(e) => onChange(e)}/>
            {closeBtn && <IconButton
                className={classes.closeBtn}
                variant={"contained"}
                color="default"
                onClick={onClose}
            >
                <Icon name="close" size={20}/>
            </IconButton> }
        </div>
    );
};

export default Uploader;