export { ReactComponent as Add } from "../../assets/icons/add.svg";
export { ReactComponent as Calender } from "../../assets/icons/calender.svg";
export { ReactComponent as Close } from "../../assets/icons/close.svg";
export { ReactComponent as Comments } from "../../assets/icons/comments.svg";
export { ReactComponent as Confirm } from "../../assets/icons/confirm.svg";
export { ReactComponent as Date } from "../../assets/icons/date.svg";
export { ReactComponent as Delete } from "../../assets/icons/delete.svg";
export { ReactComponent as Group } from "../../assets/icons/group.svg";
export { ReactComponent as Help } from "../../assets/icons/help.svg";
export { ReactComponent as SolidHelp } from "../../assets/icons/help-solid.svg";
export { ReactComponent as Home } from "../../assets/icons/home.svg";
export { ReactComponent as Lidobot } from "../../assets/icons/lidobot.svg";
export { ReactComponent as Logo } from "../../assets/icons/logo.svg";
export { ReactComponent as Logo2 } from "../../assets/icons/logo2.svg";
export { ReactComponent as Messages } from "../../assets/icons/messages.svg";
export { ReactComponent as NoImage } from "../../assets/icons/no-image.svg";
export { ReactComponent as Notification } from "../../assets/icons/notifications.svg";
export { ReactComponent as Post } from "../../assets/icons/post.svg";
export { ReactComponent as Posts } from "../../assets/icons/posts.svg";
export { ReactComponent as Refresh } from "../../assets/icons/refresh.svg";
export { ReactComponent as Setting } from "../../assets/icons/settings.svg";
export { ReactComponent as Sort } from "../../assets/icons/sort.svg";
export { ReactComponent as Statics } from "../../assets/icons/statics.svg";
export { ReactComponent as Story } from "../../assets/icons/story.svg";
export { ReactComponent as Timer } from "../../assets/icons/timer.svg";
export { ReactComponent as Users } from "../../assets/icons/users.svg";
