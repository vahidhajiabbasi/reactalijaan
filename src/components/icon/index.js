import React from 'react';

const Icon = ({name, color, size}) => {
    return (
        <i
            className={`icon-${name}`}
            style={{color:color, fontSize:size, margin:0, padding:0, lineHeight:"30px"}}
        />
    );
};

export default Icon;