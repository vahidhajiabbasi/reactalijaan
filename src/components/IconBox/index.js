import React from 'react';
import useStyles from "./Style";

const IconBox = ({children, width, height, padding}) => {
    const classes = useStyles();
    return (
        <span
            className={classes.root}
            style={{width, height, padding}}
        >
            {children}
        </span>
    );
};

export default IconBox;