import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            background:theme.colors.lightGray,
            marginLeft:20,
        },
    })
);

export default useStyles;
