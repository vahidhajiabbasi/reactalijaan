import React, {useEffect, useRef, useState} from 'react';
import {Editor, EditorState, RichUtils, convertToRaw, CompositeDecorator} from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import 'draft-js/dist/Draft.css';
import './style.css';
import { stateFromHTML } from "draft-js-import-html";
import useStyles from "./Style";
import Icon from "../icon";
import {Button, TextField} from "@material-ui/core";

const RichEditorEditor = ({html, onChange}) => {
    const classes = useStyles();

    /**
     *
     * @param contentBlock
     * @param callback
     * @param contentState
     */
    const findLinkEntities = (contentBlock, callback, contentState) => {
        contentBlock.findEntityRanges(
            (character) => {
                const entityKey = character.getEntity();
                return (
                    entityKey !== null &&
                    contentState.getEntity(entityKey).getType() === 'LINK'
                );
            },
            callback
        );
    }

    /**
     *
     * @param props
     * @returns {JSX.Element}
     * @constructor
     */
    const Link = (props) => {
        const {url} = props.contentState.getEntity(props.entityKey).getData();
        return (
            <a href={url} style={{color:"#3b5998", textDecoration: 'underline',}}>
                {props.children}
            </a>
        );
    }

    const decorator = new CompositeDecorator([
        {
            strategy: findLinkEntities,
            component: Link,
        },
    ]);

    const [editorState, setEditorState] = useState(() =>
        EditorState.createEmpty(decorator),
    );

    const [urlValue, setUrlValue] = useState('');
    const [showURLInput, setShowURLInput] = useState(false);

    const editor = useRef(null);

    function focusEditor() {
        editor.current.focus();
    }

    useEffect(()=>{
        focusEditor();
        if(html){
            setEditorState(EditorState.createWithContent(stateFromHTML(html), decorator))
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [html])

    useEffect(()=>{
        onChange(draftToHtml(convertToRaw(editorState.getCurrentContent())))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [editorState])

    const BLOCK_TYPES = [
        {label: 'UL', icon:"unorder-list", type: 'unordered-list-item'},
        {label: 'OL', icon:"order-list", type: 'ordered-list-item'},
    ];

    const INLINE_STYLES = [
        {label: 'Bold', icon:"bold", style: 'BOLD'},
        {label: 'Italic', icon:"italic", style: 'ITALIC'},
        {label: 'Underline', icon:"underline", style: 'UNDERLINE'},
    ];

    const customStyleMap = {
        FONT_SIZE_30: {
            fontSize: "30px"
        }
    };

    /**
     * get selected text from draft js editor
     */
    const promptForLink = () => {
        const selection = editorState.getSelection();
        if (!selection.isCollapsed()) {
            const contentState = editorState.getCurrentContent();
            const startKey = editorState.getSelection().getStartKey();
            const startOffset = editorState.getSelection().getStartOffset();
            const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
            const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);

            let url = '';
            if (linkKey) {
                const linkInstance = contentState.getEntity(linkKey);
                url = linkInstance.getData().url;
            }

            setUrlValue(url);
            setShowURLInput(true);
        }
    }

    /**
     * add link to selected text
     */
    const confirmLink = () => {
        const contentState = editorState.getCurrentContent();
        const contentStateWithEntity = contentState.createEntity(
            'LINK',
            'MUTABLE',
            {url: urlValue}
        );
        const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
        const newEditorState = EditorState.set(editorState, { currentContent: contentStateWithEntity });
        const nextState = RichUtils.toggleLink(
            newEditorState,
            newEditorState.getSelection(),
            entityKey
        );
        setEditorState(nextState);
        setUrlValue('');
        setShowURLInput(false);
    }

    /**
     * remove link
     */
    const removeLink = () => {
        const selection = editorState.getSelection();
        if (!selection.isCollapsed()) {
            const nextState = RichUtils.toggleLink(
                editorState,
                selection,
                null
            );
            setEditorState(nextState);
            setShowURLInput(false);
        }
    }

    /**
     * press enter key on link input
     * @param e
     */
    const onLinkInputKeyDown = (e) => {
        if (e.which === 13) {
            confirmLink();
        }
    }

    /**
     * listen to url input change
     * @param sender
     */
    const handleUrlInputChange = (sender) => {
        setUrlValue(sender.target.value);
    }

    const toggleStyle = (style) => {
        const nextState = RichUtils.toggleInlineStyle(editorState, style);
        setEditorState(nextState);
    };

    const toggleType = (type) => {
        const nextState = RichUtils.toggleBlockType(editorState, type);
        setEditorState(nextState);
    };

    /**
     * show link getter form
     * @returns {JSX.Element}
     * @constructor
     */
    const LinkInput = () => {
        return(
            <div className={classes.linkForm} >
                <p style={{marginBottom: 10, fontSize:14}}>
                    لینک دلخواه را وارد نمایید و یا با زدن دکمه حذف لینک را حذف نمایید
                </p>
                <TextField
                    variant="outlined"
                    onChange={handleUrlInputChange}
                    type="text"
                    value={urlValue}
                    onKeyDown={onLinkInputKeyDown}
                    fullWidth
                    placeholder="آدرس لینک"
                />
                <div className={classes.buttons}>
                    <Button
                        variant="contained"
                        color="primary"
                        onMouseDown={confirmLink}
                        style={{marginLeft:10}}
                    >
                        تایید
                    </Button>
                    <Button
                        variant="contained"
                        color="secondary"
                        onMouseDown={removeLink}
                    >
                        حذف لینک
                    </Button>
                </div>
            </div>
        )
    }

    return (
        <div className={classes.root}>
            <div className={classes.toolbar}>
                {
                    BLOCK_TYPES.map((type) => {
                        return(
                            <Button
                                className={classes.tools}
                                type="button"
                                onClick={()=>toggleType(type.type)}
                                key={type.label}
                            >
                                <Icon name={type.icon} size={20}/>
                            </Button>
                        )
                    })
                }
                {
                    INLINE_STYLES.map((type) => {
                        return(
                            <Button
                                className={classes.tools}
                                type="button"
                                onClick={()=>toggleStyle(type.style)}
                                key={type.label}
                            >
                                <Icon name={type.icon} size={20}/>
                            </Button>
                        )
                    })
                }
                <Button
                    className={classes.tools}
                    type="button"
                    onClick={promptForLink}
                >
                    <Icon name="link" size={20}/>
                </Button>
                {showURLInput && <LinkInput/>}
            </div>
            <div onClick={focusEditor}>
                <Editor
                    customStyleMap={customStyleMap}
                    editorState={editorState}
                    onChange={setEditorState}
                    ref={editor}
                />
            </div>
        </div>
    )
}

export default RichEditorEditor;