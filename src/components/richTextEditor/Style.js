import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            width:"100%"
        },
        toolbar:{
            position:"relative",
            padding:5,
            border:`1px solid ${theme.colors.mediumGray}`,
        },
        tools:{
            minHeight:50,
            minWidth:50,
            padding:10,
            background:"transparent",
        },
        linkForm:{
            boxShadow: `0 0 8px ${theme.colors.gray}`,
            position:"absolute",
            right:0,
            top:"110%",
            display:"flex",
            flexDirection:"column",
            zIndex:100,
            padding:20,
            maxWidth:250,
            borderRadius:5,
            background:theme.colors.white
        },
        link: {
            color: '#3b5998',
            textDecoration: 'underline',
        },
        buttons: {
            marginTop: 10,
        },
        urlInputContainer: {
            marginBottom: 10,
        },
        urlInput: {
            marginRight: 10,
            padding: 3,
        },
    })
);

export default useStyles;
