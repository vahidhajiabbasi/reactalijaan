import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            flex:"0 auto",
            width:"calc(100% - 30px)",
            padding: '15px',
            marginTop:20,
            background:theme.colors.white,
            boxShadow: `0 0 10px ${theme.colors.mediumGray}`,
            borderRadius:"5px",
            display:"flex",
            justifyContent:"space-between"
        },
        formControlContainer:{
            width:"calc(100% - 160px)"
        },
        formControl:{
            width:"100%"
        },
        squareBtn:{
            width:40,
            height:40,
            padding:0,
            background: theme.colors.lightGray,
            minWidth:30,
        }
    })
);

export default useStyles;
