import React from 'react';
import {Grid, Button, Select, FormControl, InputLabel} from "@material-ui/core";
import useStyles from './Style';
import TextField from "@material-ui/core/TextField";
import {Add} from '../../svgIcon';
import {useTheme} from "@material-ui/core/styles";
import {useHistory} from "react-router-dom/";

const PostsFilterBar = ({socialsFilter = false}) => {
    const classes = useStyles();
    const theme = useTheme();
    const history = useHistory();

    const colWidth = socialsFilter ? 3 : 4;

    return (
        <div className={classes.root}>
            <Button
                className={classes.squareBtn}
                onClick={()=>history.push('post')}
            >
                <Add fill={theme.colors.blue} width={20} height={20}/>
            </Button>
            <Grid container spacing={3} className={classes.formControlContainer}>
                <Grid item xs={12} sm={colWidth}>
                    <FormControl variant="outlined" size={"small"} className={classes.formControl}>
                        <InputLabel htmlFor="outlined-age-native-simple">وضعیت انتشار</InputLabel>
                        <Select
                            native
                            label="وضعیت انتشار"
                            inputProps={{
                                name: 'age',
                                id: 'outlined-age-native-simple',
                            }}
                        >
                            <option aria-label="None" value="" />
                            <option value={0}>پیشنویس</option>
                            <option value={1}>منتشر شده</option>
                            <option value={1}>زمان بندی شده</option>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={colWidth}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <TextField
                            size={"small"}
                            id="outlined-basic"
                            label="ازتاریخ"
                            variant="outlined"
                        />
                    </FormControl>
                </Grid>
                <Grid item xs={12} sm={colWidth}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <TextField
                            size={"small"}
                            id="outlined-basic"
                            label="تا تاریخ"
                            variant="outlined"
                        />
                    </FormControl>
                </Grid>
                { socialsFilter ? <Grid item xs={12} sm={colWidth}>
                    <FormControl variant="outlined" size={"small"} className={classes.formControl}>
                        <InputLabel htmlFor="outlined-age-native-simple">شبکه های اجتماعی</InputLabel>
                        <Select
                            native
                            label="شبکه های اجتماعی"
                            inputProps={{
                                name: 'age',
                                id: 'outlined-age-native-simple',
                            }}
                        >
                            <option aria-label="None" value="" />
                            <option value={0}>تلگرام</option>
                            <option value={1}>اینستاگرام</option>
                            <option value={2}>توئیتر</option>
                        </Select>
                    </FormControl>
                </Grid> : ''}
            </Grid>
            <Button className={classes.squareBtn}>
                <Add fill={theme.colors.blue} width={20} height={20}/>
            </Button>
            <Button className={classes.squareBtn}>
                <Add fill={theme.colors.blue} width={20} height={20}/>
            </Button>
        </div>
    );
};

export default PostsFilterBar;
