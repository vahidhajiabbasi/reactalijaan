import React from 'react';
import {
    Button,
    Card,
    CardActionArea,
    CardMedia,
    CardContent,
    CardActions
} from "@material-ui/core";
import useStyles from './Style';
import IconButton from "@material-ui/core/IconButton";
import Icon from "../../icon";
import {useHistory} from "react-router-dom/";
import {IMAGES_BASE_URL} from "../../../config";
import {Delete} from "../../../services/api";

const PostCard = ({post, onDelete}) => {

    const classes = useStyles();
    const history = useHistory();

    const deletePost = async (id) => {
        await Delete(`post/${id}`);
        onDelete(id);
    }

    return (
        <Card className={classes.root}>
                <CardActionArea
                    onClick={()=>history.push(`post/${post.Id}`)}
                >
                    <CardMedia
                        component="img"
                        alt="Contemplative Reptile"
                        height="180"
                        image={IMAGES_BASE_URL+post.image.Url}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <h2 style={{marginBottom:20}}>
                            {post.Title}
                        </h2>
                        <p>
                            {post.Excerpt}
                        </p>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <IconButton onClick={()=>deletePost(post.Id)}>
                        <Icon name="delete" size={30}/>
                    </IconButton>
                    <Button size="small" color="primary" onClick={()=>console.log('edit')}>
                        ویرایش
                    </Button>
                </CardActions>
        </Card>
    );
};

export default PostCard;
