import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            boxShadow: `0 0 10px ${theme.colors.mediumGray}`,
            padding:10
        }
    })
);

export default useStyles;
