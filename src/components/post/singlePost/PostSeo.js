import React, {useEffect, useState} from 'react';
import {FormControl} from "@material-ui/core";
import LinearProgressBar from "../../linearProgressBar";

const PostSeo = ({seoJson, onChange}) => {
    const [metaTitle, setMetaTitle] = useState("");
    const [metaDescription, setMetaDescription] = useState("");
    const [titlePercent, setTitlePercent] = useState(0)
    const [descriptionPercent, setDescriptionPercent] = useState(0)

    useEffect(()=>{
        if(seoJson){
            const fields = JSON.parse(seoJson)
            setMetaTitle(fields.title);
            setMetaDescription(fields.description);
            setTitlePercent(100 * (fields.title.length/120) )
            setTitlePercent(100 * (fields.title.description/120) )
        }
    }, [seoJson])

    useEffect(()=>{
        onChange({Seo: JSON.stringify({title:metaTitle, description:metaDescription})});
        setTitlePercent(100 * (metaTitle.length/120) );
        setDescriptionPercent(100 * (metaDescription.length/120) );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [metaTitle, metaDescription])

    return (
        <>
            <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                <label>عنوان سئو</label>
                <div>
                    <LinearProgressBar value={titlePercent} />
                </div>
                <textarea
                    value={metaTitle || ""}
                    onChange={(e)=>setMetaTitle(e.target.value)}
                />
            </FormControl>
            <FormControl variant="outlined" fullWidth>
                <label>توضیحات سئو</label>
                <div>
                    <LinearProgressBar value={descriptionPercent} />
                </div>
                <textarea
                    value={metaDescription || ""}
                    onChange={(e)=>setMetaDescription(e.target.value)}
                />
            </FormControl>
        </>
    );
};

export default PostSeo;