import React, {useEffect, useState} from 'react';
import {Get} from "../../../services/api";
import {Button, Chip, Menu, MenuItem} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "../../icon";


const PostTag = ({tags, onChange}) => {
    const classes = useStyles();
    const [postTags, setPostTags] = useState([]);
    const [allTags, setAllTags] = useState([]);
    const [anchorEl, setAnchorEl] = React.useState(null);

    useEffect(()=>{
        //get all post Tags
        const set = async () => {
            const allTags = await Get("post/tag/all?Page=1&PerPage=1000&OrderBy=Title&Order=ASC")
            if(allTags) setAllTags(allTags.Data.Rows);
            tags && setPostTags(tags.map(a => a.PostTagId));
        }
        set();
    }, [tags])

    useEffect(()=>{
        onChange({Tags:postTags});
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postTags])

    const handleDeleteChip = (id) => {
        const index = postTags.findIndex(element => element === id);
        let newTags = [...postTags];
        newTags.splice(index, 1);
        setPostTags(newTags);
    }

    const handleSelectTag = (id) => {
        setPostTags(prevState => [...prevState, id]);
        setAnchorEl(null);
    }

    return (
        <div className={classes.root}>
            <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                variant="outlined"
                color="default"
                size="small"
                endIcon={<Icon name="arrow-down" size={10}/>}
                onClick={(e)=>setAnchorEl(e.currentTarget)}
            >
                انتخاب برچسب
            </Button>
            <Menu
                id="simple-menu"
                keepMounted
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={(e)=>setAnchorEl(null)}
            >
                {allTags.map(tag => <MenuItem
                    key={tag.Id}
                    onClick={()=>handleSelectTag(tag.Id)}
                    disabled={postTags.includes(tag.Id)}
                >
                    <p>{tag.Title}</p>
                </MenuItem>)}
            </Menu>
            {postTags.map(postTag=><Chip
                key={postTag}
                label={allTags.find(tag => tag.Id === postTag).Title}
                onDelete={()=>handleDeleteChip(postTag)}
            />)}
        </div>
    );
};

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        alignItems:"center",
        justifyContent: 'right',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    },
}))

export default PostTag;