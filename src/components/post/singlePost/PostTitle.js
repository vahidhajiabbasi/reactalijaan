import React, {useEffect, useState} from 'react';
import {FormControl, TextField} from "@material-ui/core";

const PostTitle = ({title, slug, onChange}) => {

    const [postTitle, setPostTitle] = useState("")
    const [postSlug, setPostSlug] = useState("")

    useEffect(() => {
        setPostTitle(title);
        setPostSlug(slug);
    }, [title, slug]);

    useEffect(()=>{
        onChange({Title:postTitle, Slug:postSlug});
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postTitle, postSlug]);

    return (
        <>
            <FormControl variant="outlined" style={{marginBottom:20}} fullWidth>
                <TextField
                    label="عنوان"
                    variant="outlined"
                    value={postTitle || ""}
                    onChange={(e)=>setPostTitle(e.target.value)}
                />
            </FormControl>
            <FormControl variant="outlined" fullWidth>
                <TextField
                    label="پیوند یکتا"
                    variant="outlined"
                    value={postSlug || ""}
                    onChange={(e)=>setPostSlug(e.target.value)}
                />
            </FormControl>
        </>
    );
};

export default PostTitle;