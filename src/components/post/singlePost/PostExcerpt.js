import React, {useEffect, useState} from 'react';

const PostExcerpt = ({text, onChange}) => {
    const [excerpt, setExcerpt] = useState("");

    useEffect(()=>{
        setExcerpt(text);
    }, [text])

    useEffect(()=>{
        onChange({Excerpt: excerpt});
    }, [excerpt, onChange])

    return (
        <textarea
            value={excerpt}
            onChange={(e)=>setExcerpt(e.target.value)}
        />
    );
};

export default PostExcerpt;