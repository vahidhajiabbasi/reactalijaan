import React, {useEffect, useState} from 'react';
import {Get} from "../../../services/api";
import {Button, Chip, Menu, MenuItem} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Icon from "../../icon";


const PostCategory = ({categories, onChange}) => {
    const classes = useStyles();
    const [postCategories, setPostCategories] = useState([]);
    const [allCategories, setAllCategories] = useState([]);
    const [anchorEl, setAnchorEl] = React.useState(null);

    useEffect(()=>{
        //get all post categories
        const set = async () => {
            const allCategories = await Get("post/category/all?Page=1&PerPage=1000&OrderBy=Title&Order=ASC")
            if(allCategories) setAllCategories(allCategories.Data.Rows);
            categories && setPostCategories(categories.map(a => a.PostCategoryId));
        }
        set();
    }, [categories])

    useEffect(()=>{
        onChange({Categories:postCategories});
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [postCategories])

    const handleDeleteChip = (id) => {
        const index = postCategories.findIndex(element => element === id);
        let newCats = [...postCategories];
        newCats.splice(index, 1);
        setPostCategories(newCats);
    }

    const handleSelectCategory = (id) => {
        setPostCategories(prevState => [...prevState, id]);
        setAnchorEl(null);
    }

    return (
        <div className={classes.root}>
            <Button
                aria-controls="simple-menu"
                aria-haspopup="true"
                variant="outlined"
                color="default"
                size="small"
                endIcon={<Icon name="arrow-down" size={10}/>}
                onClick={(e)=>setAnchorEl(e.currentTarget)}
            >
                انتخاب دسته بندی
            </Button>
            <Menu
                id="simple-menu"
                keepMounted
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={(e)=>setAnchorEl(null)}
            >
                {allCategories.map(category => <MenuItem
                    key={category.Id}
                    onClick={()=>handleSelectCategory(category.Id)}
                    disabled={postCategories.includes(category.Id)}
                >
                    <p>{category.Title}</p>
                </MenuItem>)}
            </Menu>
            {postCategories.map(PostCategory=><Chip
                key={PostCategory}
                label={allCategories.find(cat => cat.Id === PostCategory).Title}
                onDelete={()=>handleDeleteChip(PostCategory)}
            />)}
        </div>
    );
};

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        alignItems:"center",
        justifyContent: 'right',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    },
}))

export default PostCategory;