import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
          width:"100%"
        },
        toolBars:{
            width:"100%",
            background:"white",
            borderBottom:`1px solid ${theme.colors.mediumGray}`
        },
        editor:{
            width:"100%",
            background:"white",
        }
    })
);

export default useStyles;
