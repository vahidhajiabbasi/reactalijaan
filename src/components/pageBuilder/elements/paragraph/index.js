import React, {useEffect} from 'react';
import useStyles from "./Style";
import RichEditorEditor from "../../../richTextEditor";

const Paragraph = ({content, onChange}) => {
    const classes = useStyles();
    const [html, setHtml] = React.useState('');

    /**
     * set initial for first time
     * this hook prevent loop
     */
    useEffect(()=>{
        if(content) {
            setHtml(content.html ? content.html : "");
            return
        }
        setHtml("");
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <div className={classes.root}>
            <RichEditorEditor html={html} onChange={(text)=>onChange({html:text})}/>
        </div>
    );
};

export default Paragraph;