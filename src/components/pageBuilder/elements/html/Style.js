import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            display:"flex",
            flexDirection:"column",
            width:"calc(100% - 40px)",
            margin:20,
            "& h3":{
                marginBottom: 10
            },
            "& textarea":{
                direction: "ltr !important"
            }
        },
    })
);

export default useStyles;