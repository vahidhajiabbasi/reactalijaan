import React, {useEffect, useState} from 'react';
import useStyles from "./Style";

const Index = ({content, onChange}) => {
    const classes = useStyles();
    const [html, setHtml] = useState(content ? content.html : "")

    useEffect(()=>{
        onChange({html});
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [html])

    return (
        <div className={classes.root}>
            <h3>کد اچ تی ام ال</h3>
            <textarea
                value={html}
                onChange={(e)=>setHtml(e.target.value)}
                rows={8}
            />
        </div>
    );
};

export default Index;