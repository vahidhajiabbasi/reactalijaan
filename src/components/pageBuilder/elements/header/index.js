import React, {useEffect, useState} from 'react';
import useStyles from "./Style";
import {Button, Menu, MenuItem} from "@material-ui/core";
import Icon from "../../../icon";

const Header = ({content, onChange}) => {
    const classes = useStyles();

    const [headerText, setHeaderText] = useState("");
    const [headerType, setHeaderType] = useState("");
    const [anchorEl, setAnchorEl] = React.useState(null);

    /**
     * set initial for first time
     * this hook prevent loop
     */
    useEffect(()=>{
        if(content) {
            setHeaderText(content.text ? content.text : "");
            setHeaderType(content.type ? content.type : "h1");
            return
        }
        setHeaderType("h1");
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    /**
     * listen to header text and type changes
     */
    useEffect(() => {
        onChange({text:headerText, type:headerType});
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [headerText, headerType])

    const handleHeadersButtonClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleCloseButtonOptions = () => {
        setAnchorEl(null);
    };

    const handleHeaderTypeChange = (type) => {
        setHeaderType(type);
        setAnchorEl(null);
    }

    const handleHeaderTextChange = (sender) => {
        setHeaderText(sender.target.value);
    }

    const headers = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

    return (
        <div className={classes.root}>
            <Button
                className={classes.headerButton}
                aria-controls="simple-menu"
                aria-haspopup="true"
                onClick={handleHeadersButtonClick}
            >
                <Icon name={headerType} size={20}/>
            </Button>
            <Menu
                id="simple-menu"
                keepMounted
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={handleCloseButtonOptions}
            >
                {headers.map(header => <MenuItem key={header} onClick={()=>handleHeaderTypeChange(header)}><Icon name={header} size={20}/></MenuItem>)}
            </Menu>
            <textarea
                placeholder="عنوان را وارد نمایید"
                value={headerText}
                rows={1}
                onChange={(e)=>handleHeaderTextChange(e)}/>
        </div>
    );
};

export default Header;