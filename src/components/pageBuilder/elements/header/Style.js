import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            display:"flex",
            width:"calc(100% - 40px)",
            margin:20
        },
        textArea:{
            border:`1px solid ${theme.colors.gray}`,
            borderRadius:3,
            marginTop:10,
            width:"calc(100% - 42px)",
            padding:20,
            resize:"none",
            fontSize:15,
            fontWeight:"bold"
        },
        headerButton:{
            minHeight:50,
            width:50,
            background:theme.colors.mediumGray
        }
    })
);

export default useStyles;
