import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
          margin:"10px 0"
        },
        layout:{
            width:"calc(100% - 10px)",
            border:`1px dashed ${theme.colors.mediumGray}`,
            display:"flex",
            justifyContent:"center",
            padding:5
        },
        layoutControls:{
            borderBottom:`1px solid ${theme.colors.mediumGray}`,
            marginBottom:8,
            "& button":{
                minWidth:50,
                minHeight:30,
                border:`1px solid ${theme.colors.mediumGray}`,
                borderBottom:"none",
                borderRadius:"3px 3px 0 0"
            }
        },
        toolbar:{
            borderBottom:`1px solid ${theme.colors.mediumGray}`,
            padding:"10px",
            display:"flex",
            justifyContent:"space-between",
            background:theme.colors.lightGray
        },
        layoutColumns:{
            padding:10
        },
        layoutColumn:{
            border:`1px solid ${theme.colors.mediumGray}`,
        }
    })
);

export default useStyles;
