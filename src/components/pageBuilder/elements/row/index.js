import React, {useEffect, useRef, useState} from 'react';
import useStyles from './Style';
import {Button, Divider, Grid, IconButton, Menu, MenuItem} from "@material-ui/core";
import Icon from "../../../icon";
import {v4 as Uuid} from 'uuid';
import Column from "../column";

const Row = ({contents, onChange, onDelete}) => {
    const classes = useStyles();
    const [columns, setColumns] = useState(contents.columns);
    const row = useRef(null);
    const [anchorEl, setAnchorEl] = useState(null);

    /**
     * listen to columns changes
     */
    useEffect(() => {
        contents.columns = columns;
        onChange(contents);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [columns])

    /**
     * change row layout
     * @param arrange
     */
    const handleColumnLayoutChanges = (arrange) => {
        let newColumns;
        if(!columns[0]) {
            newColumns = [];
            arrange.map(width => newColumns.push({id:Uuid(), width}));
        }
        else if(arrange.length > columns.length){
            newColumns = [...columns];
            newColumns.slice(0, newColumns.length).map((column, index)=>column.width = arrange[index]);
            arrange.slice(columns.length).map(width => newColumns.push({id:Uuid(), width}));
        }
        else if(arrange.length < columns.length){
            newColumns = [...columns];
            newColumns.slice(0, arrange.length).map((column, index)=>column.width = arrange[index]);
            newColumns = newColumns.slice(0, arrange.length);
        }
        else{
            newColumns = [...columns];
            newColumns.map((column, index)=>column.width = arrange[index]);
            newColumns = newColumns.slice(0, arrange.length);
        }
        setColumns(newColumns);
        row.current.style.border = "none";
    }

    /**
     * open row options menu
     * @param event
     */
    const handleLayoutOptionButtonClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    /**
     * close row options menu
     */
    const handleCloseLayoutOptionButton = () => {
        setAnchorEl(null);
    };

    /**
     * show layout select option
     * @returns {JSX.Element}
     * @constructor
     */
    const LayoutSelector = () => {
        const layouts = [
            {title:"یک ستون", icon:"1-layout", arrange:[12]},
            {title:"دو ستون", icon:"1-1-layout", arrange:[6, 6]},
            {title:"سه ستون", icon:"3-layout", arrange:[4, 4, 4]},
            {title:"ستون چپ باریک، ستون راست پهن", icon:"1-2-layout", arrange:[8, 4]},
            {title:"ستون راست باریک، ستون چپ پهن", icon:"refresh-copy-17", arrange:[4, 8]},
            {title:"دو ستون کنار باریک، ستون وسط پهن", icon:"1-2-1-layout", arrange:[3, 6, 3]},
        ]
        return (
            <div>
                {
                    layouts.map(layout => {
                        return <IconButton key={layout.title} onClick={()=>handleColumnLayoutChanges(layout.arrange)}>
                            <Icon name={layout.icon} />
                        </IconButton>
                    })
                }
            </div>
        )
    }

    /**
     * handle changes of column component
     * @param data
     */
    const handleColumnChanges = (data) => {
        let newColumns = [...columns];
        const colIndex = newColumns.findIndex(element => element.id === data.id);
        newColumns[colIndex] = data;
        contents.columns = newColumns;
        onChange(contents)
        setAnchorEl(null);
    }

    /**
     * delete current row
     */
    const deleteRow = () => {
        onDelete(contents.id);
        setAnchorEl(null);
    }

    return (
        <div className={classes.root}>
            <div className={classes.layoutControls}>
                <Button
                    aria-controls="layout-option-menu"
                    aria-haspopup="true"
                    onClick={handleLayoutOptionButtonClick}
                >
                    <Icon name="v-more" size={20} />
                </Button>
            </div>
            <Menu
                id="layout-option-menu"
                keepMounted
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                onClose={handleCloseLayoutOptionButton}
            >
                <MenuItem>
                    <LayoutSelector />
                </MenuItem>
                <Divider />
                <MenuItem onClick={deleteRow}>
                    <p>پاک کرد ردیف</p>
                </MenuItem>
            </Menu>
            <div ref={row} className={classes.layout}>
                {
                    !columns[0] ?
                        <LayoutSelector/> :
                        <Grid container spacing={3}>
                            {
                                columns.map((column) => {
                                    return <Column key={column.id} contents={column} onChange={handleColumnChanges} width={column.width}/>
                                })
                            }
                        </Grid>
                }
            </div>
        </div>
    );
};

export default Row;