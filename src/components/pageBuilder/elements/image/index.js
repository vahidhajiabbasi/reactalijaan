import React, {useEffect, useLayoutEffect, useRef, useState} from 'react';
import {
    Button, FormControl, IconButton, InputLabel, Select, TextField,
} from "@material-ui/core";
import useStyles from "./Style";
import Uploader from "../../../uplodaer";
import {IMAGES_BASE_URL} from "../../../../config";
import DataDialog from "../../../dialog";
import MediaDialog from "../../../mediaLibrary/MediaDialog";
import Icon from "../../../icon";

const Image = ({content, onChange}) => {
    const classes = useStyles();

    const [image, setImage] = useState(content ? content.path : null);
    const [alt, setAlt] = useState(content ? content.alt : null);
    const [size, setSize] = useState(content ? content.size : null);
    const [externalLink, setExternalLink] = useState(content ? content.image : null);
    const [showDeleteBtn, setShowDeleteBtn] = useState(false);
    const [showImageSelector, setShowImageSelector] = useState(false);
    const [showImageAddress, setShowImageAddress] = useState(false);

    const imageBox = useRef(null);
    const imageBoxMask = useRef(null);

    useLayoutEffect(()=>{
        if(!imageBox.current) return;
        imageBox.current.addEventListener("mouseover", ()=>{
            setShowDeleteBtn(true);
            imageBoxMask.current.addEventListener("mouseleave", ()=>{
                setShowDeleteBtn(false);
            });
        });
    }, []);

    const deleteImage = () => {
        setSize(null);
        setImage(null);
        setAlt(null);
        setExternalLink(null);
        setShowDeleteBtn(false);
    }

    const handleImageChange = (file) => {
        setImage(file.Path);
    }

    const imageSizes = [
        {id:"original", title:"اصلی"},
        {id:"large", title:"بزرگ"},
        {id:"medium", title:"میانه"},
        {id:"small", title:"کوچک"},
        {id:"thumb", title:"بند انگشتی"},
    ];

    /**
     * handle changes
     */
    useEffect(()=>{
        onChange({path:image, alt, size, image:externalLink});
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [image, alt, size, externalLink])

    return (
        <div className={classes.root}>
            {!image ? <>
                {/*upload new image*/}
                <Uploader onUpload={(path)=>setImage(path.Path)}/>

                {/*select image from media library*/}
                <Button
                    style={{marginTop:15}}
                    variant={"contained"}
                    fullWidth
                    color="default"
                    onClick={() => setShowImageSelector(true)}
                >
                    انتخاب از تصاویر موجود
                </Button>
                <DataDialog
                    visibility={showImageSelector}
                    onClose={()=>setShowImageSelector(false)}
                    title="رسانه ها"
                >
                    <MediaDialog onSelect={handleImageChange}/>
                </DataDialog>

                {/*enter image link address*/}
                {!showImageAddress ? <Button
                    style={{marginTop:15}}
                    variant={"contained"}
                    fullWidth
                    color="default"
                    onClick={() => setShowImageAddress(true)}
                >
                    وارد کردن آدرس تصویر
                </Button> :
                <div className={classes.imageAddress}>
                    <IconButton
                        color={"primary"}
                        onClick={()=>{setImage(externalLink)}}
                    >
                        <Icon name="confirm"/>
                    </IconButton>
                    <FormControl variant="outlined" fullWidth>
                        <TextField
                            label="آدرس تصویر را وارد نمایید"
                            variant="outlined"
                            size={"small"}
                            onChange={(e)=>setExternalLink(e.target.value)}
                        />
                    </FormControl>
                </div>}

            </> : <>
                <img src={externalLink ? image : IMAGES_BASE_URL+image} ref={imageBox} alt="test"/>
                {showDeleteBtn && <div className={classes.deleteButton} ref={imageBoxMask}>
                    <Button
                        style={{marginTop:15}}
                        variant={"contained"}
                        color="secondary"
                        onClick={deleteImage}
                    >
                        حذف تصویر
                    </Button>
                </div>}
                <FormControl variant="outlined" style={{marginTop:20}} fullWidth>
                    <TextField
                        label="متن جایگزین"
                        variant="outlined"
                        size={"small"}
                        onChange={(e)=>{setAlt(e.target.value)}}
                        value={alt}
                    />
                </FormControl>
                {!externalLink && <FormControl
                    style={{marginTop:15}}
                    variant="outlined"
                    fullWidth
                    size={"small"}
                    className={classes.formControl}
                >
                    <InputLabel htmlFor="outlined-age-native-simple">سایز تصویر</InputLabel>
                    <Select
                        native
                        label="سایز تصویر"
                        onChange={(e)=>{setSize(e.target.value)}}
                    >
                        {
                            imageSizes.map(item=>{
                                return <option value={item.id} key={item.id} defaultValue={item.id} >{item.title}</option>
                            })
                        }
                    </Select>
                </FormControl>}
            </>}
        </div>
    );
};

export default Image;