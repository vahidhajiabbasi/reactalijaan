import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            display:"flex",
            alignItems:"center",
            flexDirection:"column",
            justifyContent:"space-between",
            width:"calc(100% - 40px)",
            margin:20,
            "& img":{
                width:"100%",
            }
        },
        deleteButton:{
            position:"absolute",
            background:"rgba(0, 0, 0, 0.3)",
            width:"calc(100% - 54px)",
            height:"calc(100% - 54px)",
            top:27,
            right:27,
            display:"flex",
            justifyContent:"center",
            alignItems:"center",
            borderRadius:2
        },
        imageAddress:{
            display:"flex",
            width:"100%",
            flexDirection:"row-reverse",
            justifyContent:"center",
            alignItems:"center"
        }
    })
);

export default useStyles;
