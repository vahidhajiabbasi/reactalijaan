import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
        root:{
            position:"relative",
        },
        columnControls:{
            position:"absolute",
            left:20,
            top:-10,
        },
        content:{
            border:`1px dashed ${theme.colors.gray}`
        },
    })
);

export default useStyles;
