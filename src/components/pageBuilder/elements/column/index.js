import React, {useEffect, useState} from 'react';
import useStyles from './Style';
import {Box, Grid, IconButton} from "@material-ui/core";
import Icon from "../../../icon";
import Header from "../header";
import Paragraph from "../paragraph";
import Image from "../image";
import Html from "../html";

const Column = ({contents, width, onChange}) => {
    const classes = useStyles();

    const [column, setColumn] = useState({
        id:contents.id,
        type:contents.type,
        data:contents.data,
        width:contents.width
    });

    /**
     * listen to column changes
     */
    useEffect(()=>{
        onChange(column);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [column])

    /**
     * handle select page component
     * @param component
     */
    const selectComponent = (component) => {
        setColumn(prevState => {
           return {...prevState, type: component}
        });
    }

    /**
     * show page components toolbars
     * @returns {JSX.Element}
     * @constructor
     */
    const PageComponentsToolBar = () => {
        const componentsButton = [
            {title:"header", icon:"header"},
            {title:"paragraph", icon:"paragraph"},
            {title:"image", icon:"image"},
            {title:"video", icon:"video"},
            {title:"html", icon:"html"},
            {title:"spacer", icon:"spacer"},
        ];

        return (
            <>
                {
                    componentsButton.map(button=>{
                        return(
                            <IconButton
                                title={button.title}
                                key={button.title}
                                onClick={()=>selectComponent(button.title)}
                            >
                                <Icon name={button.icon} />
                            </IconButton>
                        )
                    })
                }
            </>
        )
    }

    /**
     * handle column component changes
     * @param data
     */
    const handleColumnDataChange = (data) => {
        onChange({...column, data: data})
    }

    /**
     *
     */
    const deleteColumnContent = () => {
        setColumn(prevState => {
            return {...prevState, type: null, data: {}}
        });
    }

    // list of components
    const components = {
        header:<Header content={column.data} onChange={handleColumnDataChange}/>,
        paragraph:<Paragraph content={column.data} onChange={handleColumnDataChange}/>,
        image:<Image content={column.data} onChange={handleColumnDataChange}/>,
        html:<Html content={column.data} onChange={handleColumnDataChange}/>,
    };

    return (
        <Grid  item xs={12} sm={12} md={12} lg={width} className={classes.root}>
            <Box display="flex" justifyContent="center" className={classes.content}>
                {!column.type ? <PageComponentsToolBar /> : components[column.type]}
            </Box>
            {column.type && <div className={classes.columnControls}>
                <IconButton onClick={deleteColumnContent}><Icon name="solid-close" color="red"/></IconButton>
            </div>}
        </Grid>
    );
};

export default Column;