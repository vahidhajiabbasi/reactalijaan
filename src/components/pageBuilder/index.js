import React, {useEffect, useState} from 'react';
import useStyles from "./Style";
import {IconButton} from "@material-ui/core";
import Icon from "../icon";
import Row from "./elements/row";
import {v4 as Uuid} from 'uuid';
import {useTheme} from "@material-ui/core/styles";
import {IMAGES_BASE_URL} from "../../config";
import {getImageSizeName, ImageSize} from "../../utils/ImageSize";

/**
 * build post page html
 * @param contents
 * @param onChange
 * @returns {JSX.Element}
 * @constructor
 */
const PageBuilder = ({contents, onChange}) => {
    const classes = useStyles();
    const theme = useTheme();

    const [rows, setRows] = useState([]);

    /**
     * set initial contents
     */
    useEffect(()=>{
        setRows(compilePostContentToJson(contents));
    }, [contents])

    /**
     * listen to rows changes
     */
    useEffect(()=>{
        onChange({Content : compileJsonToHtml(rows)});
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [rows])

    /**
     * add new empty row
     */
    const addRow = () => {
        setRows(prevState => [...prevState, {
            id:Uuid(),
            type:"row",
            columns:[]
        }])
    }

    /**
     * delete row
     * @param id
     * @constructor
     */
    const HandleDeleteRow = (id) => {
        const rowIndex = rows.findIndex(element => element.id === id);
        let newRows = [...rows];
        newRows.splice(rowIndex, 1);
        setRows(newRows);
    }

    /**
     * handle rows changes
     * @param changes
     */
    const handleChanges = (changes) => {
        const index = rows.findIndex(element => element.id === changes.id);
        let tempEditor = [...rows];
        if(changes.action === "delete") {
            tempEditor.splice(index,1);
        } else {
            tempEditor[index].columns = changes.columns;
        }
        setRows(tempEditor);
    }

    /**
     * convert html post content to json object
     * @param content
     * @returns {[]}
     */
    const compilePostContentToJson = (content) => {

        if(!content) return [];

        let obj=[];

        const findType = (classes) => {
            const types = ['header', 'paragraph', 'image', 'html']
            return types.filter(value => classes.includes(value))[0];
        }

        const findWidth = (classes) => {
            const widthClass = classes.filter(value => value.includes("col-md"))[0];
            if(widthClass){
                const parts = widthClass.split("-");
                return parseInt(parts[2]);
            }
        }

        const getData = (type, col) => {
            switch (type){
                case "header":
                    const head = col.children[0]
                    return {text:head.innerHTML, type:head.tagName};

                case "paragraph":
                    return {html : col.innerHTML};

                case "image":
                    const image = col.children[0];
                    const src = image.getAttribute('src');
                    const alt = image.getAttribute('alt');
                    const className = image.className;
                    if(className.includes("e-image")){
                        return {path:src, alt:alt, size:null, image:src}
                    } else {
                        const imageInfo = getImageSizeName(src);
                        return {path:imageInfo.path, alt:alt, size:imageInfo.size}
                    }

                case "html":
                    return {html : col.innerHTML};

                default:
                    return {}
            }
        }

        const parser = new DOMParser();
        const docNode = parser.parseFromString(content,"text/xml");
        const nodeChildren = docNode.children[0].children;
        for (let i = 0; i < nodeChildren.length; i++){
            const node = nodeChildren[i].children;
            let columns = [];
            for (let j = 0; j < node.length; j++){
                const col = node[j];
                const columnClasses = col.attributes.class.value.split(' ');
                const type = findType(columnClasses)
                columns.push({
                    id:Uuid(),
                    type: type,
                    data:getData(type, col),
                    width:findWidth(columnClasses)
                });
            }
            obj.push({id:Uuid(), type:"row", columns})
        }
        return obj;
    }

    /**
     * convert json contents to html
     * @param editor
     * @returns {*}
     */
    const compileJsonToHtml = (editor) => {
        if(!editor) return null;

        const getRowHtml = (row) => {
            const columns = row.columns.map(col=>{
                return `<div class="col-md-${12/row.columns.length} ${col.type}">${getColumnHtml(col)}</div>`
            })
            return columns.join('');
        }

        const getColumnHtml = (col) => {
            if(!col.data) return null;
            switch (col.type){
                case "paragraph":
                    return col.data.html;

                case "header":
                    return `<${col.data.type}>${col.data.text}</${col.data.type}>`;

                case "image":
                    return !col.data.image?
                        `<img src="${IMAGES_BASE_URL+ImageSize(col.data.path, col.data.size)}" alt="${col.data.alt}"/>`:
                        `<img class="e-image" src="${col.data.path}" alt="${col.data.alt}"/>`;

                case "html":
                    return col.data.html;

                default:
                    return null;
            }
        }

        const html = editor.map((row)=>{
            return `<div class="row">${getRowHtml(row)}</div>`;
        });
        return '<div>'+html.join('')+'</div>';
    }

    return (
        <div className={classes.root}>
            <div className={classes.editor} id="editor">
                {
                    rows && rows.map(row=>{
                        return <Row
                            key={row.id}
                            contents={row}
                            onChange={handleChanges}
                            onDelete={HandleDeleteRow}
                        />
                    })
                }
            </div>
            <IconButton
                title="add"
                onClick={()=>addRow()}
            >
                <Icon name="add" color={theme.colors.blue}/>
            </IconButton>
        </div>
    )
}

export default PageBuilder;