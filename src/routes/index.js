import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Register from "../pages/auth/Register";
import AuthLayout from "../components/authLayout";
import Password from "../pages/auth/Password";
import Dashboard from "../pages/Dashboard";
import Otp from "../pages/auth/Otp";
import { AppRoutes } from "./AppRoutes";
import MainLayout from "../components/mainLayout";
import WebsiteDashboard from "../pages/Website";
import AdminDashboard from "../pages/admin";
import Posts from "../pages/Website/posts";
import SinglePost from "../pages/Website/singlePost";
import MediaLibrary from "../pages/Website/mediaLibrary";
import PostCategory from "../pages/Website/categories";
import PostTag from "../pages/Website/tags";

const Routes = () => {
    return (
        <Switch>
            <Route>
                <AppRoutes path="/"  exact layout={MainLayout} component={Dashboard} />
                <AppRoutes path="/website"  exact layout={MainLayout} component={WebsiteDashboard} />
                <AppRoutes path="/website/posts"  exact layout={MainLayout} component={Posts} />
                <AppRoutes path="/website/post"  exact layout={MainLayout} component={SinglePost} />
                <AppRoutes path="/website/media"  exact layout={MainLayout} component={MediaLibrary} />
                <AppRoutes path="/website/categories"  exact layout={MainLayout} component={PostCategory} />
                <AppRoutes path="/website/tags"  exact layout={MainLayout} component={PostTag} />
                <AppRoutes path="/website/post/:id"  exact layout={MainLayout} component={SinglePost} />
                <AppRoutes path="/admin"  exact layout={MainLayout} component={AdminDashboard} />
                <AppRoutes path="/user/register-login" layout={AuthLayout} auth={false} component={Register} />
                <AppRoutes path="/user/login/password" layout={AuthLayout} auth={false} component={Password} />
                <AppRoutes path="/user/login/confirm" layout={AuthLayout} auth={false} component={Otp} />
            </Route>
        </Switch>
    );
};

export default Routes;
