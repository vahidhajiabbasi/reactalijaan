import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const AppRoutes = ({component: Component, layout: Layout, auth = true,  ...rest}) => {
    return <Route
        {...rest}
        render={(props) =>
            auth && !localStorage.getItem('token') ?
                <Redirect to="/user/register-login"/> :
                <Layout>
                    <Component {...props} />
                </Layout>
        }
    />
};
