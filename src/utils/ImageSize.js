import {IMAGES_BASE_URL} from "../config";

export const ImageSize = (imageUrl, sizeName) => {
    if(!sizeName || !imageUrl) return  "";
    const lastDotIndex = imageUrl.lastIndexOf(".");
    const extension = imageUrl.slice(lastDotIndex).toLowerCase().substring(1);
    if(imageExtensions.includes(extension)){
        return imageUrl.slice(0, lastDotIndex) + "_" +sizes[sizeName] + imageUrl.slice(lastDotIndex);
    }
    switch (extension){
        case 'pdf':
            return "/pdf.png";
        case 'mp4':
            return "/video.png";
        default:
            return "";
    }
}

export const getImageSizeName = (imageUrl) => {
    if(!imageUrl) return "original";
    const lastUnderLineIndex = imageUrl.lastIndexOf("_");
    const lastDotIndex = imageUrl.lastIndexOf(".");
    const size = imageUrl.slice(lastUnderLineIndex+1, lastDotIndex);
    if(sizesName[size] !== undefined){
        return {
            path: imageUrl.replace(IMAGES_BASE_URL, '').replace("_"+size, ''),
            size:sizesName[size]
        };
    } else {
        return {
            path: imageUrl.replace(IMAGES_BASE_URL, ''),
            size:"original"
        };
    }
}

const imageExtensions = ['jpeg', 'jpg', 'png'];
const sizes = {thumb:150, small:540, medium:960, large:1100, original:''}
const sizesName = {"150":"thumb", "540":"small", "960":"medium", "1100":"large"}
