import Swal from 'sweetalert2'

export const alert = (error) => {
    Swal.fire(JSON.stringify(error))
};
