import Axios from "axios";
import { alert } from './HandleErrors'
import {API_URL} from "../../config";

const token = localStorage.getItem('token');
const options = {
    baseURL: API_URL,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer "+token
    }
};

const api = Axios.create(options);

const Get = (url, errorCallback) => {

    return new Promise(resolve => {
        api.get(url)
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                alert(error)
            })
    });

};

const Delete = (url, errorCallback) => {

    return new Promise(resolve => {
        api.delete(url)
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                alert(error)
            })
    });

};

const Post = (url, body, errorCallback) => {

    return new Promise(resolve => {
        api.post(url, body)
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                alert(error)
            })
    });

};

const Put = (url, body, errorCallback) => {

    return new Promise(resolve => {
        api.put(url, body)
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                alert(error)
            })
    });

};

const Upload = (url, bodyFormData, errorCallback) => {
    return new Promise(resolve => {
        Axios({
            method: 'post',
            url: options.baseURL + url,
            data: bodyFormData,
            headers: {
                'Content-Type': 'multipart/form-data',
                "Authorization": "Bearer "+token
            }
        })
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                alert(error)
            })
    });
}

export { Post, Get, Put, Upload, Delete };

