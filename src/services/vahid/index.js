import Axios from "axios";
import { errorHandling } from './HandleErrors'
import {API_URL} from "../../config";

const getHeaders = (serverSide) => {
    let headers = {
        Accept: "application/json",
        "Content-Type": "application/json"
    }
    if(!serverSide) {
        const token = sessionStorage.getItem('token');
        headers =  {
            ...headers,
            "Authorization": token ? token : null
        }
    }
    return headers;
}
const Get = (payload, errorCallback) => {

    return new Promise(resolve => {
        Axios.get( `${API_URL}${payload.url}`,{
            headers: getHeaders(payload.serverSide)
        })
            .then(response => {
                resolve(response.data)
            })
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                errorHandling(error)
            })
    });
};

const Delete = (payload, errorCallback) => {

    return new Promise(resolve => {
        Axios.delete(`${API_URL}${payload.url}`,{
            headers: getHeaders(payload.serverSide)
        })
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                errorHandling(error)
            })
    });

};

const Post = (payload, body, errorCallback) => {

    return new Promise(resolve => {
        Axios.post(`${API_URL}${payload.url}`, body,{
            headers: getHeaders(payload.serverSide)
        })
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                errorHandling(error)
            })
    });
};

const Put = (payload, body, errorCallback) => {

    return new Promise(resolve => {
        Axios.put(`${API_URL}${payload.url}`, body,{
            headers: getHeaders(payload.serverSide)
        })
            .then(response => {resolve(response.data)})
            .catch(error => {
                if(errorCallback) {
                    errorCallback(error);
                    return;
                }
                errorHandling(error)
            })
    });

};

const Upload = (payload, bodyFormData, errorCallback) => {
    // return new Promise(resolve => {
    //     Axios({
    //         method: 'post',
    //         url: `${API_URL}${payload.url}`,
    //         data: bodyFormData,
    //         headers: {
    //             'Content-Type': 'multipart/form-data',
    //             "Authorization": "Bearer "+token
    //         }
    //     })
    //         .then(response => {resolve(response.data)})
    //         .catch(error => {
    //             if(errorCallback) {
    //                 errorCallback(error);
    //                 return;
    //             }
    //             errorHandling(error)
    //         })
    // });
}

export { Post, Get, Put, Upload, Delete };

