import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const theme = createMuiTheme({
    direction: 'rtl',
    palette: {
        primary: {
            main: "#017aff",
        },
        secondary: {
            main: "#F64369",
        },
    },
    colors: {
        blue: "#017aff",
        lightBlue: "#cae3fd",
        darkBlue: "#353d6b",
        gray: "#9599ae",
        mediumGray: "#e7e8ee",
        lightGray: "#f4faff",
        backgroundGray: "#f5f7fb",
        red: "#f64369",
        green: "#34d69a",
        white: "#ffffff",
    },
    overrides: {
        MuiFormLabel:{
            root:{
                right:0,
                left:"initial !important",
                transform: "translate(-15px, 85%) scale(1) !important"
            },
        },
        MuiInputLabel:{
            shrink:{
                transform: "translate(0, -6px) scale(0.75) !important",
                right:"15px !important",
                left:"initial !important",
                transformOrigin: "top right"
            }
        },
        MuiSelect: {
            icon: {
                left:7,
                right:"initial"
            },
            iconOutlined: {
                left:7,
                right:"initial"
            },
        },
        PrivateNotchedOutline:{
            legendLabelled:{
                textAlign:"right",
            }
        },
        MuiAccordion:{
            root:{
                boxShadow: "0 0 10px #e7e8ee",
            }
        },
        MuiAccordionSummary:{
            content:{
                alignItems:"center"
            }
        },
        MuiChip: {
            deleteIcon:{
                margin: "0 5px 0 5px"
            }
        },
        MuiButton: {
            endIcon:{
                marginLeft: "0px !important",
                marginRight: "8px !important"
            }
        },
        MuiDialogContent:{
            root:{
                padding:20
            }
        },
        MuiListItemText:{
            root:{
                textAlign:"right"
            }
        },
        MuiListItemSecondaryAction:{
            root:{
                left:16,
                right:"initial"
            }
        },
        MuiListItem:{
            secondaryAction: {
                paddingRight:"initial",
                paddingLeft:48,
            }
        }
    },
});

export default theme;
