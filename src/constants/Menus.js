export const Menus = {
    '/':[
        {
            title: 'وب سایت',
            permissions : ['get_posts', 'create_tag', 'get_tags', 'get_categories', 'create_category', 'get_posts', 'create_post'],
            path: "/website",
            icon:"posts"
        },
        {
            title: 'مدیریت',
            permissions : ['change_permission', 'create_role'],
            path: "/admin",
            icon:"users"
        },
    ],
    '/website':[
        {
            title: 'نوشته ها',
            permissions : ['get_posts', 'create_post'],
            path: "/website/posts",
            icon:"posts"
        },
        {
            title: 'رسانه ها',
            permissions : [],
            path: "/website/media",
            icon:"calender"
        },
        {
            title: 'دسته ها',
            permissions : ['get_categories', 'create_category'],
            path: "/website/categories",
            icon:"post"
        },
        {
            title: 'برچسب ها',
            permissions : ['create_tag', 'get_tags'],
            path: "/website/tags",
            icon:"story"
        },
        {
            title: 'دیدگاه ها',
            permissions : [],
            path: "/website/comments",
            icon:"comments"
        },
    ]
};
