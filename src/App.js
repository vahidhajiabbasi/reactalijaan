import React, {Component} from 'react';
import Routes from "./routes";
import { ThemeProvider } from "@material-ui/core";
import theme from "./globalStyles/theme";
import './globalStyles/fonts.css';
import './globalStyles/global.css';
import './globalStyles/lido.css';


class App extends Component {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <Routes />
            </ThemeProvider>
        );
    }
}

export default App;
